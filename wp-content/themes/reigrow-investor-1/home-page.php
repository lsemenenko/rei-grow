<?php
/* Template Name: Home Page */

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php astra_content_page_loop(); ?>
    

<?php echo do_shortcode('[reim_hero_content_slideshow]'); ?>
        
        <div class="section">
        <div class="container rei-main-container">
      
      <?php echo do_shortcode('[reim_main_content]'); ?>
           
</div>   
        </div>
        
        
        <?php echo do_shortcode('[reim_quicklinks]'); ?>
        
                <?php if( get_field('rei_home_projects_status') == 'show'): ?>
         <div class="rei-projects-container">
             <div class="container-fluid rei-main-container">
             <?php echo do_shortcode('[reim_projects]
'); ?>
                 <?php echo do_shortcode('[projects count=0]
'); ?>
        </div></div>
        <?php endif; ?>
        
        <?php if( get_field('reim_home_reviews_status') == 'show'): ?>
         <div class="rei-reviews-container">
             <div class="container-fluid rei-main-container">
             <?php echo do_shortcode('[reim_reviews]
'); ?>
                 <?php echo do_shortcode('[reviews count=0]
'); ?>
        </div></div>
        <?php endif; ?>
        
         <div class="rei-faq-container">
             <div class="container-fluid rei-main-container">
             <?php echo do_shortcode('[reim_faq]
'); ?>
        </div></div>
        
		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
