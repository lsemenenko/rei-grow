<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

?>

			<?php astra_content_bottom(); ?>

			</div> <!-- ast-container -->

		</div><!-- #content -->

		<?php astra_content_after(); ?>

		<?php astra_footer_before(); ?>

<div class="rei-footer-section">
<div class="container-fluid pr-4 pl-4 rei-footer-container">
        <div class="row">
  
        <div class="col-lg-4 rei-footer-business-info mb-5">
            <h3><?php echo get_bloginfo( 'name' ); ?>
</h3>
            <div><?php echo comicpress_copyright(); ?> - <?php echo get_field('rei_theme_options_business_info_legal_name', 'options'); ?></div>
            <br/>
            <div>
                <?php echo get_field('rei_theme_options_business_info_street_address', 'options'); ?> <div><?php echo get_field('rei_theme_options_business_info_city', 'options'); ?>, <?php echo get_field('rei_theme_options_business_info_state', 'options'); ?> <?php echo get_field('rei_theme_options_business_info_zip', 'options'); ?> 
            </div>
                <br/>
                <div class="rei-phone-number">
                    <a href="tel:<?php echo get_field('rei_theme_options_business_info_phone', 'options'); ?>"> <?php echo get_field('rei_theme_options_business_info_phone', 'options'); ?> </a>
                </div>
                </div>
            </div>
        <div class="col-lg-2 mb-5">
           <?php wp_nav_menu(
  array(
    'menu' => 'footer-1',
      'menu_class' => 'rei-footer-menu-container',
  )
); ?></div>
        <div class="col-lg-3 mb-5">
            <?php wp_nav_menu(
  array(
    'menu' => 'footer-2',
      'menu_class' => 'rei-footer-menu-container',
  )
); ?></div>
        <div class="col-lg-3 mb-5 rei-social-icons-container">
        
            <?php $field = get_field('rei_social_media_facebook_link', 'options') ; if (!empty($field)) { ?>
            <div class="rei-social-icon-wrap"><span class="rei-social-icon"><a target="_blank" href="<?php echo get_field('rei_social_media_facebook_link', 'options'); ?>" class="fab fa-facebook-square"></a></span><div class="rei-social-icon-text"><a target="_blank" href="<?php echo get_field('rei_social_media_facebook_link', 'options'); ?>" class="rei-social-icon-text-style">Facebook</a></div></div>
            <?php } ?>
            
            <?php $field = get_field('rei_social_media_twitter_link', 'options') ; if (!empty($field)) { ?>
            <div class="rei-social-icon-wrap"><span class="rei-social-icon"><a target="_blank" href="<?php echo get_field('rei_social_media_twitter_link', 'options'); ?>" class="fab fa-twitter-square"></a></span><div class="rei-social-icon-text"><a target="_blank" href="<?php echo get_field('rei_social_media_twitter_link', 'options'); ?>" class="rei-social-icon-text-style">Twitter</a></div></div>
            <?php } ?>
            
            <?php $field = get_field('rei_social_media_instagram_link', 'options') ; if (!empty($field)) { ?>
            <div class="rei-social-icon-wrap"><span class="rei-social-icon"><a target="_blank" href="<?php echo get_field('rei_social_media_instagram_link', 'options'); ?>" class="fab fa-instagram"></a></span><div class="rei-social-icon-text"><a target="_blank" href="<?php echo get_field('rei_social_media_instagram_link', 'options'); ?>" class="rei-social-icon-text-style">Instagram</a></div></div>
            <?php } ?>
            
            <?php $field = get_field('rei_social_media_linkedin_link', 'options') ; if (!empty($field)) { ?>
            <div class="rei-social-icon-wrap"><span class="rei-social-icon"><a target="_blank" href="<?php echo get_field('rei_social_media_linkedin_link', 'options'); ?>" class="fab fa-linkedin"></a></span><div class="rei-social-icon-text"><a target="_blank" href="<?php echo get_field('rei_social_media_linkedin_link', 'options'); ?>" class="rei-social-icon-text-style">LinkedIn</a></div></div>
            <?php } ?>
            
            </div>
            
</div>
    <hr>
    <div class="container-fluid pt-3 p-0 rei-legal-text"><?php echo get_field('rei_footer_options_legal_text', 'option'); ?>
</div>
    </div>
    </div>
		<?php astra_footer_after(); ?>

	</div><!-- #page -->

	<?php astra_body_bottom(); ?>

	<?php wp_footer(); ?>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js">
		</script> 
<script>
jQuery(function($) {
$('.navbar .dropdown').hover(function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

}, function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

});

$('.navbar .dropdown > a').click(function(){
location.href = this.href;
});

});
</script>

	</body>
</html>
