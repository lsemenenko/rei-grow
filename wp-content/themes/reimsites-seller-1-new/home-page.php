<?php
/* Template Name: Home Page */

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php astra_content_page_loop(); ?>
        
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
            <?php 
        
    echo do_shortcode('[reim_hero_content]');
        
?>
        
        <div class="container-fluid pr-4 pl-4 rei-main-container">
        <div class="row">
  <div class="col-lg-8">
      
      <?php echo do_shortcode('[reim_main_content]'); ?>
        <span class="reim_magnet_arrow"><h3>Just Complete This Quick Form!</h3><img class="reim-arrow" style="width:175px; height:75px" src="<?php echo get_stylesheet_directory_uri();?>/assets/lead-magnet-arrow.png"></span>
            </div>
            
  <div class="col-lg-4">
      <div class="p-5 rei_lead_magnet_bg">
      <h3>
          <?php echo get_field('rei_s1_lead_magnet_options_title', 'options'); ?>
      </h3>
      <p class="rei-lead-magnet-description"><?php echo get_field('rei_s1_lead_magnet_options_paragraph', 'options'); ?></p>
      <?php echo do_shortcode('[gravityform id=3 title=false]
'); ?>
            </div></div>
</div>
</div>
        <?php if( get_field('reim_home_reviews_status') == 'show'): ?>
         <div class="rei-reviews-container">
             <div class="container-fluid rei-main-container">
             <?php echo do_shortcode('[reim_reviews]
'); ?>
                 <?php echo do_shortcode('[reviews count=0]
'); ?>
        </div></div>
        <?php endif; ?>
        
         <div class="rei-faq-container">
             <div class="container-fluid rei-main-container">
             <?php echo do_shortcode('[reim_faq]
'); ?>
        </div></div>
        
		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
