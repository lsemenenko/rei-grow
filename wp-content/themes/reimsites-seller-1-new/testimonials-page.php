<?php
/* Template Name: Testimonials Page */

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php astra_content_page_loop(); ?>
        
                   <?php 
        
    echo do_shortcode('[reim_hero_content]');
        
?>
        <div class="rei-testimonials-main">
        <div class="container-fluid pr-4 pl-4 rei-main-container">
        <div class="row">
  <div class="col-lg-8">
      
      <?php echo do_shortcode('[reim_main_content]'); ?>
      
         <?php echo do_shortcode('[reviews count=0]
'); ?>
      
          <?php $field = get_field('rei_add_form_toggle') ; if (!empty($field)) { ?>
      <?php echo do_shortcode('[reim_add_form_title]
'); ?>
      <?php $form = get_field('rei_add_form_select');
gravity_form($form['id'], false, true, false, '', true, 1); 
 ?>
      <?php echo do_shortcode('[reim_add_form_content]
'); ?>
        <?php } ?>
            </div>
            
  <div class="col-lg-4">
      <div class="p-5 rei_lead_magnet_bg">
      <h3>
          <?php echo get_field('rei_s1_lead_magnet_options_title', 'options'); ?>
      </h3>
      <p class="rei-lead-magnet-description"><?php echo get_field('rei_s1_lead_magnet_options_paragraph', 'options'); ?></p>
      <?php echo do_shortcode('[gravityform id=3 title=false]
'); ?>
            </div></div>
</div>
</div>
            </div>


		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
