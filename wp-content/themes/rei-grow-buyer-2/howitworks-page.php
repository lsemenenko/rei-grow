<?php
/* Template Name: How it Works Page */

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php astra_content_page_loop(); ?>
        
                          <?php 
        
    echo do_shortcode('[reim_hero_content_color]');
        
?>
        
        <div class="container-fluid rei-main-container">
        <div class="row container-fluid">
  <div class="col-lg-8 pr-4 pl-4 mb-5">
      
      <?php echo do_shortcode('[reim_main_content]'); ?>
        
            </div>
            
  <div class="col-lg-4 px-0">
      <div class="p-5 rei_lead_magnet_bg">
      <h3>
          <?php echo get_field('rei_b1_lead_magnet_options_header', 'options'); ?>
      </h3>
      <p class="rei-lead-magnet-description"><?php echo get_field('rei_b1_lead_magnet_options_description', 'options'); ?></p>
      <?php echo do_shortcode('[gravityform id=3 title=false]
'); ?>
            </div></div>
</div>
</div>

		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
