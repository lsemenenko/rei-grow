<?php
/**
 * REIMSites Buyer 4 Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package REI Grow Buyer 4
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_REIMSITES_SELLER_1_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'reimsites-seller-1-theme-css', get_stylesheet_directory_uri() . '/style.css?v=1.6', array('astra-theme-css'), CHILD_THEME_REIMSITES_SELLER_1_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

// Hide Lead Magnet Page
add_filter( 'parse_query', 'ts_hide_pages_in_wp_admin' );
function ts_hide_pages_in_wp_admin($query) {
    global $pagenow,$post_type;
    if (is_admin() && $pagenow=='edit.php' && $post_type =='page') {
        $query->query_vars['post__not_in'] = array('379');
    }
}
add_filter('wp_list_pages_excludes', 'exclude_from_wp_list_pages');
 function exclude_from_wp_list_pages($exclude_array){
    $exclude_array = $exclude_array + array('379');
    return $exclude_array;
 }

add_action( 'init', function() {
    remove_post_type_support( 'page', 'editor' );
}, 99);


// Include the ACF plugin.
include_once( get_stylesheet_directory() . '/includes/acf/add-acf-fields.php' );

function comicpress_copyright() {
global $wpdb;
$copyright_dates = $wpdb->get_results("
SELECT
YEAR(min(post_date_gmt)) AS firstdate,
YEAR(max(post_date_gmt)) AS lastdate
FROM
$wpdb->posts
WHERE
post_status = 'publish'
");
$output = '';
if($copyright_dates) {
$copyright = "&copy; " . $copyright_dates[0]->firstdate;
if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
$copyright .= '-' . $copyright_dates[0]->lastdate;
}
$output = $copyright;
}
return $output;
}

// Register Custom Navigation Walker
require_once get_stylesheet_directory() . '/includes/class-wp-bootstrap-navwalker.php';

// Site Options
require_once get_stylesheet_directory() . '/includes/class-rei-sites-insert-header.php';

function custom_menu_page_removing() {
        remove_menu_page( 'edit.php?post_type=rei_projects' );
}
add_action( 'admin_menu', 'custom_menu_page_removing' );