<?php
/* Template Name: Home Page */

get_header(); ?><?php if ( astra_page_layout() == 'left-sidebar' ) : ?><?php get_sidebar(); ?><?php endif ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div id="primary">
		<?php 
		        
		    echo do_shortcode('[reim_hero_content_lead_magnet_s2]');
		        
		?>
		<div class="container-fluid rei-main-content-home">
			<div class="row">
				<div class="container-fluid col-lg-8">
					<div class="container-fluid pt-0 pb-5 p-0">  <?php echo do_shortcode('[reim_main_content]'); ?>
     
      <?php $field = get_field('rei_add_form_toggle') ; if (!empty($field)) { ?>
      <?php echo do_shortcode('[reim_add_form_title]
'); ?>
      <?php $form = get_field('rei_add_form_select');
gravity_form($form['id'], false, true, false, '', true, 1); 
 ?>
      <?php echo do_shortcode('[reim_add_form_content]
'); ?>
        <?php } ?></div>
				                <div class="rei-faqs-container p-0">
			<div class="container-fluid p-0">
				<?php echo do_shortcode('[reim_faq]
				'); ?>
			</div>
                </div>

                    </div>
				 
                <?php if( get_field('reim_home_reviews_status') == 'show'): ?>
                <div class="col-lg-4">
					<div class="rei-reviews-container p-4 py-5 mt-0 mb-0">
			<div class="container-fluid">
				<?php echo do_shortcode('[reim_reviews]
				'); ?><?php echo do_shortcode('[reviews count=0]
				'); ?>
			</div>
		</div>
				</div>
                    <?php endif; ?>
			</div>
		</div>
		
		</div><?php astra_primary_content_bottom(); ?>
	</div><!-- #primary -->
	<?php if ( astra_page_layout() == 'right-sidebar' ) : ?><?php get_sidebar(); ?><?php endif ?><?php get_footer(); ?>
</body>
</html>