<?php
/* Template Name: Gravity Form Page */

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php astra_content_page_loop(); ?>
        <div style="max-width: 1200px; margin-left: auto; margin-right: auto; margin-top: 75px; margin-bottom: 75px; padding-right:20px; padding-left: 20px;">
               <h2> <?php echo get_field('rei_sites_options_lead_magnet_header', 'options'); ?> </h2>
            
            <?php 
        
    echo do_shortcode('[gravityform id=4 title=false description=false ajax=true tabindex=49]');
        
?></div>

		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
