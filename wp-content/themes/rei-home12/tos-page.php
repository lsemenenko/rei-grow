<?php
/* Template Name: TOS Page */


get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>
            
 <div style="max-width: 1200px; margin-left: auto; margin-right: auto; margin-top: 75px; margin-bottom: 75px; padding-right:20px; padding-left: 20px;">
     <h2><?php single_post_title(); ?></h2>
<?php 
        
    echo do_shortcode('[reim_main_content]');
     ?>
    
     
		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
