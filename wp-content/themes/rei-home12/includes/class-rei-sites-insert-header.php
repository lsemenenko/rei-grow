<?php
class Rei_Sites_insert_header {
    
    

    function __construct(){
        
        
    add_action('wp_head', array($this, 'hook_css'));
        
        
    }
    
    
    
    public function hook_css() {
    
    if (get_field('rei_s1_theme_colors','options')) {
    $rei_s1_theme_colors_group = get_field('rei_s1_theme_colors','options')?get_field('rei_s1_theme_colors','options'):'' ;
    $rei_s1_theme_options_group = get_field('rei_s1_theme_options','options')?get_field('rei_s1_theme_colors','options'):'' ;
    $header_color = $rei_s1_theme_colors_group['header_text_color']?$rei_s1_theme_colors_group['header_text_color']:'' ;
    $reiHeroImageID = get_field('field_5d13dfe84c0a8', get_the_ID())['image']?get_field('rei_s1_theme_colors','options'):'' ;
         $heroImageSize = 'full';
         $reiHeroImageURL = wp_get_attachment_image_url( $reiHeroImageID, $heroImageSize );
        
    $styleCss = '<style>.navbar-light .navbar-nav .nav-link, .dropdown-item{
            color: '.$header_color.' !important;
            }
        .menu a:hover {
            color: '.$rei_s1_theme_colors_group['link_color'].' ; 
        
        }';
        if(isset($rei_s1_theme_options_group['bbb_logo'])){
            $styleCss .= '.rei_bbb_logo {
                display: '.$rei_s1_theme_options_group['bbb_logo'].' ; 
                }';

        }
        if(isset($rei_s1_theme_options_group['link_color'])){
            $styleCss .= '.reim_header_phone_number {
                color: '.$rei_s1_theme_colors_group['link_color'].' ; 
            }
            i.fas.fa-phone, .fa-tty { 
                color: '.$rei_s1_theme_colors_group['link_color'].' ; 
            }';

        }
        
        $styleCss .= '.main-header-menu a, .ast-header-custom-item a, .main-header-menu li:hover > a, .main-header-menu li:hover > .ast-menu-toggle, .main-header-menu .ast-masthead-custom-menu-items a:hover, .main-header-menu li.focus > a, .main-header-menu li.focus > .ast-menu-toggle, .main-header-menu .current-menu-item > a, .main-header-menu .current-menu-ancestor > a, .main-header-menu .current_page_item > a, .main-header-menu .current-menu-item > .ast-menu-toggle, .main-header-menu .current-menu-ancestor > .ast-menu-toggle, .main-header-menu .current_page_item > .ast-menu-toggle {
            color: '.$rei_s1_theme_colors_group['header_text_color'].' !important;
        }
        .ast-footer-overlay {
            background-color: '.$rei_s1_theme_colors_group['footer_background_color'].';
            }
        input[type=submit] {
        background-color: '.$rei_s1_theme_colors_group['button_color'].';
        }  
        input[type=submit]:hover {
            background-color: '.$rei_s1_theme_colors_group['button_color'].';
        }
        
        .fl-builder-content a.fl-button {
            background-color: '.$rei_s1_theme_colors_group['button_color'].';
        
        }
        .content-area a {
            color: '.$rei_s1_theme_colors_group['link_color'].' ; 
        }
        .col-lg-4 a, .col-lg-4 a:hover, .col-lg-4 a:focus {
    color: '.$rei_s1_theme_colors_group['link_color'].';
}
        a:hover, a:focus {
              color: '.$rei_s1_theme_colors_group['link_color'].' ; 
        }
        .rei-faq-container, .reim-faq-question-container, .reim-faq-question-container-last {
        background-color: '.$rei_s1_theme_colors_group['faq_background_color'].' ;
        }
        .rei-footer-section {
        background-color: '.$rei_s1_theme_colors_group['footer_background_color'].' ;
        }';
        if(isset($rei_s1_theme_colors_group['button_color'])){
            $styleCss .= 'a.fl-button {
                background-color: '.$rei_s1_theme_colors_group['button_color'].' !important;
                }
            .reim_faq_button {
                background-color: '.$rei_s1_theme_colors_group['button_color'].' !important;
            }';

        }
        if(isset($rei_s1_theme_colors_group['lead_magnet_background_color'])){
            $styleCss .= '.rei_lead_magnet_bg {
                background-color: '.$rei_s1_theme_colors_group['lead_magnet_background_color'].';
                }';

        }
        if(isset($rei_s1_theme_colors_group['lead_magnet_text_color'])){
            $styleCss .= '.rei_lead_magnet_bg {
                color: '.$rei_s1_theme_colors_group['lead_magnet_text_color'].';
                }';

        }
        if(isset($rei_s1_theme_colors_group['hero_background_color'])){
            $styleCss .= '.rpp-profile-image {
                background: linear-gradient(90deg,'.$rei_s1_theme_colors_group['hero_background_color'].',rgba(0,0,0,0.35)), url('.$reiHeroImageURL.') ;
                }';

        }
        
        $styleCss .= 'li#wp-admin-bar-wpshapere_site_title{
            width: 100px !important;
            padding-top: 5px !important;
            padding-left: 10px !important;
        }
        .ast-right-sidebar #secondary {
            padding-left: 0px;
        }
        .fl-node-5d0cd8b86f6bc {
            background: #f1f1f1;
        }
        .ast-right-sidebar #primary {
            border-right: 0px;
        }
        .ast-right-sidebar #secondary {
            border-left: 0px !important;
        }
        .rei_hero_title {
        color: #fff !important;
        }
        .rei_hero_subtitle {
        color: #fff !important;
        }
        </style>';}


        }
    
    
} //end class
    
    new Rei_Sites_insert_header;