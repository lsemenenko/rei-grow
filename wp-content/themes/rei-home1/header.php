<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>

 <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo get_field('rei_tracking_codes_ga', 'option') ?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?php echo get_field('rei_tracking_codes_ga', 'option') ?>');
</script>
    
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','<?php echo get_field('rei_tracking_codes_gtm', 'option') ?>');</script>
<!-- End Google Tag Manager -->    
 
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '<?php echo get_field('rei_tracking_codes_fbpm', 'option') ?>');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=<?php echo get_field('rei_tracking_codes_fbpm', 'option') ?>&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

    
<?php astra_head_top(); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">

<?php wp_head(); ?>
<?php astra_head_bottom(); ?>
    
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">  
    


</head>

<body <?php astra_schema_body(); ?> <?php body_class(); ?>>
    
    <?php echo get_field('rei_sa_tracking_code'); ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo get_field('rei_tracking_codes_gtm', 'option') ?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php astra_body_top(); ?>
<?php wp_body_open(); ?>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>

	<?php astra_header_before(); ?>

    
<nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top shadow-sm" role="navigation">
  <div class="container">
	<!-- Brand and toggle get grouped for better mobile display -->
      <a class="navbar-brand" href="/"><img class="rei-logo-image" src="<?php echo get_field('rei_theme_options_logo_image', 'options') ?> "></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
          
	</button>
      
      <?php
		wp_nav_menu( array(
			'theme_location'    => 'primary',
			'depth'             => 2,
			'container'         => 'div',
			'container_class'   => 'collapse navbar-collapse',
			'container_id'      => 'bs-example-navbar-collapse-1',
			'menu_class'        => 'nav navbar-nav',
			'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			'walker'            => new WP_Bootstrap_Navwalker(),
		) );
		?>
     
    <div><div class="rei-header-module"><img class="rei_bbb_logo" src="https://venturamural.com/wp-content/uploads/2019/07/bbb-logo.png" style="width:80px"></div><?php $field = get_field('rei_theme_options_business_info_phone', 'options') ; if (!empty($field)) { ?><div class="rei-header-module"><a href="tel:<?php echo get_field('rei_theme_options_business_info_phone', 'options'); ?>"><i style="margin-right:10px; font-size: 24px; vertical-align: middle" class="fas fa-phone"></i><span class="reim_header_phone_number" style="vertical-align: middle"><?php echo get_field('rei_theme_options_business_info_phone', 'options'); ?></span></a></div> <?php } ?> </div></div>
</nav>

	<?php astra_header_after(); ?>

	<?php astra_content_before(); ?>

	<div id="content" class="site-content">

		<div class="ast-container">

            <?php echo do_shortcode('[frontend_editor_bar]'); ?>
		<?php astra_content_top(); ?>
