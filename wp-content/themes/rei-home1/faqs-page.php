<?php
/* Template Name: Frequently Asked Questions Page */

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php astra_content_page_loop(); ?>
        
                                 <?php 
        
    echo do_shortcode('[reim_hero_content]');
        
?>
        
        <div class="container-fluid pr-4 pl-4 rei-main-container">
        <div class="row">
  <div class="col-lg-8">
      
      <?php echo do_shortcode('[reim_main_content]'); ?>
        
            </div>
            
  <div class="col-lg-4">
      <div class="p-5 rei_lead_magnet_bg">
      <h3>
          <?php echo get_field('rei_s1_theme_options_lead_magnet_header', 'options'); ?>
      </h3>
      
      <?php echo do_shortcode('[gravityform id=2 title=false]
'); ?>
            </div></div>
</div>
</div>


		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
