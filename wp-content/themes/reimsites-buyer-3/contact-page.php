<?php
/* Template Name: Contact Page */


get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>
         
        <div style="max-width: 800px; margin-left: auto; margin-right: auto; margin-top: 75px; margin-bottom: 75px; padding-right:20px; padding-left: 20px;">
                <?php echo do_shortcode('[reim_main_content]
'); ?>
       <?php $field = get_field('rei_add_form_toggle') ; if (!empty($field)) { ?>
      <?php echo do_shortcode('[reim_add_form_title]
'); ?>
      <?php $form = get_field('rei_add_form_select');
gravity_form($form['id'], false, true, false, '', true, 1); 
 ?>
      <?php echo do_shortcode('[reim_add_form_content]
'); ?>
        <?php } ?></div>


		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
