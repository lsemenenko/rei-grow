msgid ""
msgstr ""
"Project-Id-Version: WPShapere\n"
"POT-Creation-Date: 2016-10-08 20:53+0530\n"
"PO-Revision-Date: 2016-10-13 14:40+0530\n"
"Last-Translator: AcmeeDesign <support@acmeedesign.com>\n"
"Language-Team: AcmeeDesign softwares and solutions\n"
"Language: en_001\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.9\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: .\n"

#: includes/acmee-framework/inc/aof.class.php:43
#, php-format
msgid "%s Settings"
msgstr "%s Paramètres"

#: includes/acmee-framework/inc/aof.class.php:59
msgid "Loading..."
msgstr "Chargement..."

#: includes/acmee-framework/inc/aof.class.php:177
msgid "Invalid Purchase code! Or the envato server may be down. "
msgstr ""
"Code d'achat non valide ! Ou le serveur envato est peut-être hors service. "

#: includes/acmee-framework/inc/aof.class.php:178
msgid ""
"Or your server may not support wordpress remote get method. Please open a "
"support ticket at "
msgstr ""
"Ou votre serveur ne supporte pas la méthode distant wordpress. S'il vous "
"plaît ouvrir un ticket de support à"

#: includes/acmee-framework/inc/aof.class.php:183
msgid "Enter Purchase Code"
msgstr "Entrez le code d'achat"

#: includes/acmee-framework/inc/aof.class.php:201
msgid "Your Purchase code is accepted. Happy customizing WordPress!"
msgstr "Votre code d'achat est acceptée. Bonne personnalisation WordPress!"

#: includes/acmee-framework/inc/aof.class.php:203
msgid "Need help?"
msgstr "Besoin d'aide?"

#: includes/acmee-framework/inc/aof.class.php:456
msgid "Font Family"
msgstr "Polices\tFamille"

#: includes/acmee-framework/inc/aof.class.php:474
msgid "Color"
msgstr "Couleur"

#: includes/acmee-framework/inc/aof.class.php:479
msgid "Font Weight"
msgstr "Poids de police "

#: includes/acmee-framework/inc/aof.class.php:489
msgid "Font Style"
msgstr "Style de police"

#: includes/acmee-framework/inc/aof.class.php:499
msgid "Font Size"
msgstr "Taille de police"

#: includes/acmee-framework/inc/aof.class.php:574
msgid "Save Changes"
msgstr "Enregistrer les modifications"

#: includes/acmee-framework/inc/aof.class.php:703
msgid "Settings saved."
msgstr "Paramètres Enregistré"

#: includes/acmee-framework/inc/aof.class.php:707
msgid "Error: Settings not saved."
msgstr "Erreur: Paramètres non enregistré."

#: includes/wps-options.php:26 includes/wps-options.php:40
msgid "General Options"
msgstr "Options Générales"

#: includes/wps-options.php:27 includes/wps-options.php:167
msgid "Login Options"
msgstr "Options de Connexion"

#: includes/wps-options.php:28 includes/wps-options.php:350
msgid "Dashboard Options"
msgstr "Options Tableau de bord"

#: includes/wps-options.php:29 includes/wps-options.php:564
msgid "Adminbar Options"
msgstr "Options bar d’Admin"

#: includes/wps-options.php:30 includes/wps-options.php:656
msgid "Admin Page Options"
msgstr "Options page d'Administrateur "

#: includes/wps-options.php:31 includes/wps-options.php:928
msgid "Admin menu Options"
msgstr "Options Menu  d'administration"

#: includes/wps-options.php:32 includes/wps-options.php:1007
msgid "Footer Options"
msgstr "Options  pied de page"

#: includes/wps-options.php:33 includes/wps-options.php:1021
msgid "Email Options"
msgstr "Options de messagerie"

#: includes/wps-options.php:45
msgid "Choose design type"
msgstr "Choisissez le type de design"

#: includes/wps-options.php:49
msgid "Flat design"
msgstr "Design plat"

#: includes/wps-options.php:50
msgid "Default design"
msgstr "Design par défaut"

#: includes/wps-options.php:56
msgid "Heading H1 color"
msgstr "Rubrique couleur H1"

#: includes/wps-options.php:63
msgid "Heading H2 color"
msgstr "Rubrique couleur H2"

#: includes/wps-options.php:70
msgid "Heading H3 color"
msgstr "Rubrique couleur H3"

#: includes/wps-options.php:77
msgid "Heading H4 color"
msgstr "Rubrique couleur H4"

#: includes/wps-options.php:84
msgid "Heading H5 color"
msgstr "Rubrique couleur H5"

#: includes/wps-options.php:91
msgid "Heading H6 color"
msgstr "Rubrique couleur H6"

#: includes/wps-options.php:98
msgid "Remove unwanted items"
msgstr "Supprimer les éléments indésirables\t"

#: includes/wps-options.php:101 includes/wps-options.php:358
msgid "Select whichever you want to remove."
msgstr "Sélectionnez celui que vous souhaitez supprimer."

#: includes/wps-options.php:103
msgid "Wordpress Help tab."
msgstr "Onglet Aide Wordpress"

#: includes/wps-options.php:104
msgid "Screen Options."
msgstr "Options de l'écran."

#: includes/wps-options.php:105
msgid "Wordpress update notifications."
msgstr "Notifications de mise à jour Wordpress"

#: includes/wps-options.php:110
msgid "Disable automatic updates"
msgstr "Désactiver les mises à jour automatiques"

#: includes/wps-options.php:113
msgid "Select to disable all automatic background updates."
msgstr ""
"Sélectionnez cette option pour désactiver toutes les mises à jour "
"automatiqueen arrière-plan. "

#: includes/wps-options.php:118
msgid "Disable update emails"
msgstr "Désactiver la mise à jour des e-mails"

#: includes/wps-options.php:121
msgid "Select to disable emails regarding automatic updates."
msgstr ""
"Sélectionnez cette option pour désactiver les e-mails concernant les mises à "
"jour automatiques"

#: includes/wps-options.php:126
msgid "Hide Admin bar"
msgstr "Masquer la barre Administrateur"

#: includes/wps-options.php:129
msgid "Select to hideadmin bar on frontend."
msgstr ""
"Sélectionner cette option pour masquer la barre Admin à l'extrémité frontal"

#: includes/wps-options.php:134
msgid "Hide Color picker from user profile"
msgstr "Masquer le sélecteur de couleurs du profil utilisateur"

#: includes/wps-options.php:137
msgid "Select to hide Color picker from user profile."
msgstr ""
"Sélectionnez cette option pour masquer le sélecteur de couleurs  du profil "
"d'utilisateur"

#: includes/wps-options.php:142
msgid "Menu Customization options"
msgstr "Options de personnalisation des menus"

#: includes/wps-options.php:147
msgid "Menu display"
msgstr "Affichage du menu"

#: includes/wps-options.php:151
msgid "Show all Menu links to all admin users"
msgstr "Afficher tous les liens du menu pour tous les utilisateurs admin"

#: includes/wps-options.php:152
msgid "Show all Menu links to specific admin users"
msgstr ""
"Afficher tous les liens du menu pour les utilisateurs administrateurs "
"spécifiques"

#: includes/wps-options.php:157
msgid "Select Privilege users"
msgstr "Sélectionnez les Utilisateurs Privilégient"

#: includes/wps-options.php:160
msgid "Select admin users who can have access to all menu items."
msgstr ""
"Sélectionnez les  utilisateurs Admin qui peuvent avoir accès à tous les "
"éléments de menu"

#: includes/wps-options.php:172
msgid "Disable custom styles for login page."
msgstr "Désactiver les styles personnalisés pour la page de connexion"

#: includes/wps-options.php:175
msgid "Check to disable"
msgstr "Cocher pour désactiver"

#: includes/wps-options.php:180 includes/wps-options.php:666
msgid "Background color"
msgstr "Couleur d'arrière-plan"

#: includes/wps-options.php:187
msgid "Background image"
msgstr "Image en arrière-plan"

#: includes/wps-options.php:193
msgid "Background Repeat"
msgstr "Répétez l'arrière-plan"

#: includes/wps-options.php:196
msgid "Check to repeat"
msgstr "Cocher pour répéter"

#: includes/wps-options.php:201
msgid "Scale background image"
msgstr "Echelle de Image de fond "

#: includes/wps-options.php:204
msgid "Scale image to fit Screen size."
msgstr "Ajuster Image de fond pour s'adapter la taille de l’écran"

#: includes/wps-options.php:209
msgid "Login Form Top margin"
msgstr "Formulaire de connexion marge haut"

#: includes/wps-options.php:218
msgid "Login Form Width in %"
msgstr "Formulaire de connexion Largeur en  %"

#: includes/wps-options.php:227 includes/wps-options.php:569
msgid "Upload Logo"
msgstr "Télécharger Logo "

#: includes/wps-options.php:230
msgid ""
"Image to be displayed on login page. Maximum width should be under 450pixels."
msgstr ""
"Image à afficher sur la page de connexion. Largeur maximale doit être sous "
"450pixels"

#: includes/wps-options.php:234
msgid "Resize Logo?"
msgstr "Redimensionner le logo ?"

#: includes/wps-options.php:238
msgid "Select to resize logo size."
msgstr "Sélectionnez cette option pour redimensionner la taille du logo."

#: includes/wps-options.php:242
msgid "Set Logo size in %"
msgstr "Définir la taille du logo en  %"

#: includes/wps-options.php:250
msgid "Logo Height"
msgstr "Hauteur du logo"

#: includes/wps-options.php:258
msgid "Logo url"
msgstr "Url du logo"

#: includes/wps-options.php:265
msgid "Transparent Form"
msgstr "Forme transparente"

#: includes/wps-options.php:269
msgid "Select to show transparent form background."
msgstr "Sélectionnez cette option pour afficher l'arrière-plan transparent"

#: includes/wps-options.php:273
msgid "Login div bacground color"
msgstr "Couleur de fond Connexion div"

#: includes/wps-options.php:280
msgid "Login form bacground color"
msgstr "Couleur de fond de Connexion"

#: includes/wps-options.php:287
msgid "Form border color"
msgstr "Couleur de bordure"

#: includes/wps-options.php:294
msgid "Form text color"
msgstr "Couleur du texte"

#: includes/wps-options.php:301
msgid "Form link color"
msgstr "Couleur de lien"

#: includes/wps-options.php:308
msgid "Form link hover color"
msgstr "Couleur de forme du lien hover "

#: includes/wps-options.php:315
msgid "Hide Back to blog link"
msgstr "Masquer en lien de blog"

#: includes/wps-options.php:319 includes/wps-options.php:327
msgid "select to hide"
msgstr "sélectionnez cette option pour masquer"

#: includes/wps-options.php:323
msgid "Hide Remember me"
msgstr "Masquez Se souvenir de moi"

#: includes/wps-options.php:331
msgid "Custom Footer content"
msgstr "Contenu personnalisé  de pied de page"

#: includes/wps-options.php:337 includes/wps-options.php:916
msgid "Custom CSS"
msgstr "CSS personnalisé"

#: includes/wps-options.php:342
msgid "Custom CSS for Login page"
msgstr "CSS personnalisé pour la page de connexion"

#: includes/wps-options.php:355
msgid "Remove unwanted Widgets"
msgstr "Supprimer les Widgets indésirables"

#: includes/wps-options.php:360
msgid "Welcome panel"
msgstr "panneau de bienvenue"

#: includes/wps-options.php:361
msgid "Right now"
msgstr "Maintenant"

#: includes/wps-options.php:362
msgid "Recent activity"
msgstr "Activité récente\t"

#: includes/wps-options.php:363
msgid "Incoming links"
msgstr "Liens entrants"

#: includes/wps-options.php:364
msgid "Plugins"
msgstr "Plugins"

#: includes/wps-options.php:365
msgid "Quick press"
msgstr "Quick press"

#: includes/wps-options.php:366
msgid "Recent drafts"
msgstr "Projets récents"

#: includes/wps-options.php:367
msgid "Wordpress news"
msgstr "Actualités Wordpress"

#: includes/wps-options.php:368
msgid "Wordpress blog"
msgstr "Blog Wordpress"

#: includes/wps-options.php:369
msgid "bbPress"
msgstr "bbPress"

#: includes/wps-options.php:370
msgid "Yoast seo"
msgstr "Yoast seo"

#: includes/wps-options.php:371
msgid "Gravity forms"
msgstr "Gravity forms"

#: includes/wps-options.php:377
msgid "Create New Widgets"
msgstr "Créer de nouveau Widgets"

#: includes/wps-options.php:383
msgid "Widget 1"
msgstr "Widget 1"

#: includes/wps-options.php:387 includes/wps-options.php:432
#: includes/wps-options.php:477 includes/wps-options.php:522
msgid "Widget Type"
msgstr "Type de Widget "

#: includes/wps-options.php:390 includes/wps-options.php:435
#: includes/wps-options.php:480 includes/wps-options.php:525
msgid "RSS Feed"
msgstr "Flux RSS"

#: includes/wps-options.php:391 includes/wps-options.php:436
#: includes/wps-options.php:481 includes/wps-options.php:526
msgid "Text Content"
msgstr "Contenu du texte"

#: includes/wps-options.php:398 includes/wps-options.php:443
#: includes/wps-options.php:488 includes/wps-options.php:533
msgid "Widget Position"
msgstr "Position Widget"

#: includes/wps-options.php:401 includes/wps-options.php:446
#: includes/wps-options.php:491 includes/wps-options.php:536
msgid "Left"
msgstr "Gauche"

#: includes/wps-options.php:402 includes/wps-options.php:447
#: includes/wps-options.php:492 includes/wps-options.php:537
msgid "Right"
msgstr "Droite"

#: includes/wps-options.php:408 includes/wps-options.php:453
#: includes/wps-options.php:498 includes/wps-options.php:543
msgid "Widget Title"
msgstr "Titre Widget"

#: includes/wps-options.php:414 includes/wps-options.php:459
#: includes/wps-options.php:504 includes/wps-options.php:549
msgid "RSS Feed url"
msgstr "URL Flux RSS"

#: includes/wps-options.php:417 includes/wps-options.php:462
#: includes/wps-options.php:507 includes/wps-options.php:552
msgid ""
"Put your RSS feed url here if you want to show your own RSS feeds. Otherwise "
"fill your static contents in the below editor."
msgstr ""
"Mettez l’URL de votre flux RSS ici si vous voulez montrer à vos propres flux "
"RSS. Sinon remplissez vos contenus statiques dans l'éditeur ci-dessous"

#: includes/wps-options.php:421 includes/wps-options.php:466
#: includes/wps-options.php:511 includes/wps-options.php:556
msgid "Widget Content"
msgstr "Contenu du widget"

#: includes/wps-options.php:428
msgid "Widget 2"
msgstr "Widget 2"

#: includes/wps-options.php:473
msgid "Widget 3"
msgstr "Widget 3"

#: includes/wps-options.php:518
msgid "Widget 4"
msgstr "Widget 4"

#: includes/wps-options.php:572
msgid "Image to be displayed in all pages. Maximum size 200x50 pixels."
msgstr "Image à afficher dans toutes les pages. Taille maximale 200x50 pixels."

#: includes/wps-options.php:576
msgid "Move logo Top by"
msgstr "Déplacer le logo du haut de"

#: includes/wps-options.php:579 includes/wps-options.php:588
msgid "Can be used in case of logo position haven't matched the menu position."
msgstr ""
"Peut être utilisé au cas où la position du logo ne s’accorde pas avec la "
"position du menu."

#: includes/wps-options.php:585
msgid "Move logo Bottom by"
msgstr "Déplacer logo du bas de"

#: includes/wps-options.php:594
msgid "Admin bar color"
msgstr "Couleur de la barre Admin"

#: includes/wps-options.php:601
msgid "Menu Link color"
msgstr "Couleur du Menu  lien"

#: includes/wps-options.php:608
msgid "Menu Link hover color"
msgstr "Couleur du menu lien hover"

#: includes/wps-options.php:615
msgid "Menu Link background hover color"
msgstr "Couleur de fond du Menu lien  hover"

#: includes/wps-options.php:622
msgid "Submenu Link color"
msgstr "Couleur du lien sous-menu"

#: includes/wps-options.php:629
msgid "Submenu Link hover color"
msgstr "Couleur lien du sous-menu hover"

#: includes/wps-options.php:636
msgid "Remove Unwanted Menus"
msgstr "Supprimer les Menus non désirés"

#: includes/wps-options.php:639
msgid "Select menu items to remove."
msgstr "Sélectionnez les éléments du menu à supprimer"

#: includes/wps-options.php:641
msgid "Site Name"
msgstr "Nom du site"

#: includes/wps-options.php:642
msgid "Updates"
msgstr "Mises à jour"

#: includes/wps-options.php:643
msgid "Comments"
msgstr "Commentaires"

#: includes/wps-options.php:644
msgid "New Content"
msgstr "Nouveau contenu"

#: includes/wps-options.php:645
msgid "Edit Profile"
msgstr "Modifier le profil"

#: includes/wps-options.php:646
msgid "My account"
msgstr "Mon compte"

#: includes/wps-options.php:647
msgid "WordPress Logo"
msgstr "Logo WordPress"

#: includes/wps-options.php:661
msgid "Page background color"
msgstr "Couleur de fond de page"

#: includes/wps-options.php:673
msgid "Primary button colors"
msgstr "Couleurs du bouton principal"

#: includes/wps-options.php:678
msgid "Button background  color"
msgstr "Couleur du bouton d’arrière-plan "

#: includes/wps-options.php:686 includes/wps-options.php:751
msgid "Button border color"
msgstr "Couleur de bordure du bouton"

#: includes/wps-options.php:693 includes/wps-options.php:758
msgid "Button shadow color"
msgstr "Couleur de l’ombre du bouton"

#: includes/wps-options.php:701 includes/wps-options.php:766
#: includes/wps-options.php:822
msgid "Button text color"
msgstr "Couleur du texte du bouton"

#: includes/wps-options.php:708 includes/wps-options.php:773
#: includes/wps-options.php:815
msgid "Button hover background color"
msgstr "Couleur d’arrière-plan du bouton hover"

#: includes/wps-options.php:716 includes/wps-options.php:781
msgid "Button hover border color"
msgstr "Couleur de bordure du bouton hover"

#: includes/wps-options.php:723 includes/wps-options.php:788
msgid "Button hover shadow color"
msgstr "Couleur d'ombre du Bouton hover"

#: includes/wps-options.php:731 includes/wps-options.php:796
#: includes/wps-options.php:829
msgid "Button hover text color"
msgstr "Couleur du texte bouton hover"

#: includes/wps-options.php:738
msgid "Secondary button colors"
msgstr "Couleurs secondaire du bouton "

#: includes/wps-options.php:743 includes/wps-options.php:808
msgid "Button background color"
msgstr "Couleur d’arrière-plan du bouton"

#: includes/wps-options.php:803
msgid "Add New button"
msgstr "Ajouter un nouveau bouton"

#: includes/wps-options.php:836
msgid "Metabox Colors"
msgstr "Couleurs de METABOX"

#: includes/wps-options.php:841
msgid "Metabox header box"
msgstr "Zone d’en-tête METABOX"

#: includes/wps-options.php:848
msgid "Metabox header box border"
msgstr "bordure de zone en-tête METABOX"

#: includes/wps-options.php:855
msgid "Metabox header Click button color"
msgstr "Couleur du bouton en-tête METABOX "

#: includes/wps-options.php:862
msgid "Metabox header Click button hover color"
msgstr "Couleur hover du bouton en-tête METABOX "

#: includes/wps-options.php:869
msgid "Metabox header text color"
msgstr "Couleur de texte en-tête METABOX"

#: includes/wps-options.php:876
msgid "Message box (Post/Page updates)"
msgstr "Boîte de message (mises à jour Post/Page)"

#: includes/wps-options.php:881
msgid "Message box color"
msgstr "Couleur de boîte de message"

#: includes/wps-options.php:888
msgid "Message text color"
msgstr "Couleur de texte de message"

#: includes/wps-options.php:895
msgid "Message box border color"
msgstr "Couleur de bordure boîte de message"

#: includes/wps-options.php:902
msgid "Message link color"
msgstr "Couleur des liens message"

#: includes/wps-options.php:909
msgid "Message link hover color"
msgstr "Message couleur lien hover"

#: includes/wps-options.php:921
msgid "Custom CSS for Admin pages"
msgstr "CSS personnalisées pour les pages Admin"

#: includes/wps-options.php:933
msgid "Admin menu width"
msgstr "Largeur du menu Admin"

#: includes/wps-options.php:942
msgid "Admin Menu Color options"
msgstr "Options de couleur du Menu admin"

#: includes/wps-options.php:947
msgid "Left menu wrap color"
msgstr "Couleur wrap du Menu Gauche"

#: includes/wps-options.php:954
msgid "Submenu wrap color"
msgstr "Couleur  wrap du sous-menus"

#: includes/wps-options.php:961
msgid "Menu hover color"
msgstr "Couleur Menu hover"

#: includes/wps-options.php:968
msgid "Current active Menu color"
msgstr "Couleur actif du Menu actuel"

#: includes/wps-options.php:975
msgid "Menu text color"
msgstr "Couleur  du texte du Menu"

#: includes/wps-options.php:982
msgid "Menu hover text color"
msgstr "Couleur de texte  du Menu hover "

#: includes/wps-options.php:989
msgid "Updates Count notification background"
msgstr "Nombre de Notification de mises à jour en arrière-plan de "

#: includes/wps-options.php:996
msgid "Updates Count text color"
msgstr "Couleur du texte Nombre de mises à jour"

#: includes/wps-options.php:1012
msgid "Footer Text"
msgstr "Texte de pied de page"

#: includes/wps-options.php:1015
msgid "Put any text you want to show on admin footer."
msgstr ""
"Mettez le texte que vous souhaitez afficher sur le pied de page "
"administrateur"

#: includes/wps-options.php:1026
msgid "White Label emails"
msgstr "emails White Label"

#: includes/wps-options.php:1029
msgid "Disable White Label emails"
msgstr "Désactiver les emails White Label "

#: includes/wps-options.php:1030
#, php-format
msgid "Set Email address as <strong> %1$s </strong> From name as <strong> %2$s"
msgstr ""
"Définir adresse E-mail comme <strong>%1$ s</strong> de nom comme <strong>"
"%2$ s"

#: includes/wps-options.php:1031
msgid "Set different"
msgstr "Définir différentes"

#: includes/wps-options.php:1038
msgid "Email From address"
msgstr "Email  Adresse De"

#: includes/wps-options.php:1041
msgid "Enter valid email address"
msgstr "Entrez une adresse e-mail valide"

#: includes/wps-options.php:1045
msgid "Email From name"
msgstr "Email De nom"

#: includes/wpshapere.class.php:68
msgid "Welcome to WPShapere "
msgstr "Bienvenue sur WPShapere"

#: includes/wpshapere.class.php:70
msgid "Visit Knowledgebase"
msgstr "Visitez la base de connaissances"

#: includes/wpshapere.class.php:230 includes/wpshapere.class.php:245
msgid "Customize Admin Menus"
msgstr "Personnaliser  les Menus Administrateur "

#: includes/wpshapere.class.php:231
msgid "Import and Export Settings"
msgstr "Importer et Exporter des Paramètres "

#: includes/wpshapere.class.php:231
msgid "Import-Export Settings"
msgstr "Paramètres d’Import-Export"

#: includes/wpshapere.class.php:247
msgid "By default, all menu items will be shown to administrator users. "
msgstr ""
"Par défaut, tous les éléments de menu seront affichés aux utilisateurs "
"administrateur"

#: includes/wpshapere.class.php:249
msgid "Click here "
msgstr "Cliquez ici"

#: includes/wpshapere.class.php:251
msgid "to customize who can access to all menu items."
msgstr "pour personnaliser qui peut accéder à tous les éléments de menu."

#: includes/wpshapere.class.php:428
msgid "Export Settings <span>Save the below contents to a text file.</span>"
msgstr ""
"Exporter des Paramètres  <span> Enregistrer le contenu ci-dessous dans un "
"fichier texte. </ Span>"

#: includes/wpshapere.class.php:432 includes/wpshapere.class.php:443
msgid "Import Settings"
msgstr " Importer des Paramètres"

#: includes/wpshapere.class.php:436
msgid "Settings Imported."
msgstr "Paramètres importés"

#: includes/wpshapere.class.php:689
#, php-format
msgid "Howdy, %1$s"
msgstr "Howdy, %1$s"

#: includes/wpsthemes.class.php:22
msgid "Import Themes"
msgstr "Importer des thèmes"

#: includes/wpsthemes.class.php:32
msgid "Import a Theme"
msgstr "Importer un thème"

#: includes/wpsthemes.class.php:33
msgid "Note: Importing a theme will replace your existing custom set colors."
msgstr ""
"Remarque :Importer un thème remplacera  remplacera vos couleurs "
"personnalisées."
