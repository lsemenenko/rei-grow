<?php

/**
 * Fired during plugin activation
 *
 * @link       https://idbmedia.com
 * @since      1.0.0
 *
 * @package    Rei_Sites
 * @subpackage Rei_Sites/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Rei_Sites
 * @subpackage Rei_Sites/includes
 * @author     J Hanlon - IDB Media <j@roselane.com>
 */
class Rei_Sites_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
