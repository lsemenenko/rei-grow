<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://idbmedia.com
 * @since      1.0.0
 *
 * @package    Rei_Sites
 * @subpackage Rei_Sites/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Rei_Sites
 * @subpackage Rei_Sites/includes
 * @author     J Hanlon - IDB Media <j@roselane.com>
 */
class Rei_Sites {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Rei_Sites_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'REI_SITES_VERSION' ) ) {
			$this->version = REI_SITES_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'rei-sites';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
    
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Rei_Sites_Loader. Orchestrates the hooks of the plugin.
	 * - Rei_Sites_i18n. Defines internationalization functionality.
	 * - Rei_Sites_Admin. Defines all hooks for the admin area.
	 * - Rei_Sites_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
        

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rei-sites-loader.php';
        

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rei-sites-i18n.php';
        
        /**
		 * The class responsible for defining all shortcodes used by the builder.
		 */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rei-shortcode-builder.php';

		/**
		 * The classes responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rei-sites-admin.php';

		//Not Used. May use in future.
		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rei-sites-add-network-pages.php';
		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rei-sites-user-site-settings-page.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rei-sites-review-cpt.php';
        
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rei-sites-review-shortcode.php';
        
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rei-sites-project-cpt.php';
        
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rei-sites-project-shortcode.php';

		/**
		 * The classes responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rei-front-styles.php';
		//add posts and pages
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rei-resume-page.php';
		// add ajax frontend save
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/ajax-save-post/idb_ajax_save_resume.php';    
			   
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rei-sites-add-acf-fields.php';  
			 
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rei-sites-add-frontend-forms.php'; 

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rei-sites-public.php';
   
		$this->loader = new Rei_Sites_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Rei_Sites_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Rei_Sites_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

    
    
	
    
	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
        
        //admin styles and scripts
        $plugin_admin = new Rei_Sites_Admin( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );

		//Not used may use in the future.
		//$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        
		// Custom profile page and site settings. NOT USED: MAY USE IN FUTURE
        // $subsite_settings = new reiUserSiteSettingsPage( $this->get_plugin_name(), $this->get_version() );
        // $this->loader->add_action('admin_menu', $subsite_settings, 'register_profile_page' );
        // $this->loader->add_action('acf/init', $subsite_settings, 'register_site_options_sub_page');
        //network admin menu
        //$add_plugin_pages = new reiAddNetworkPages( $this->get_plugin_name(), $this->get_version() );
        
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Rei_Sites_Public( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
        
        // add shortcode builder
        
        
        $shortcode_builder = new IdbBuilderShortcodes( $this->get_plugin_name(), $this->get_version() );
        
        
        //register reviews cpt
        $register_reviews_cpt = new Rei_Sites_Reviews_CPT( $this->get_plugin_name(), $this->get_version() );
        $this->loader->add_action( 'init', $register_reviews_cpt, 'reviews_post_type', 0);
        
        $public_reviews_functions = new Rei_Sites_Review_Shortcode( $this->get_plugin_name(), $this->get_version() );
        $this->loader->add_action( 'init', $public_reviews_functions, 'rating_average_func', 1);
        $this->loader->add_action( 'init', $public_reviews_functions, 'rating_average_rounded_func', 1);
        
        //register projects cpt
        $register_projects_cpt = new Rei_Sites_Projects_CPT( $this->get_plugin_name(), $this->get_version() );
        $this->loader->add_action( 'init', $register_projects_cpt, 'projects_post_type', 0);
		
		//check this and see if we still need it for shortcodes
		$public_projects_functions = new Rei_Sites_Project_Shortcode( $this->get_plugin_name(), $this->get_version() );
        
        // add frontend forms
        $frontend_forms = new Rei_Sites_FrontendForms( $this->get_plugin_name(), $this->get_version() );
        if(is_user_logged_in()):
        $this->loader->add_action('wp_enqueue_scripts', $frontend_forms, 'add_acf_head');
        $this->loader->add_filter('astra_footer_after', $frontend_forms, 'idb_rei_acf_form_content');
        $this->loader->add_filter('astra_header_before', $frontend_forms, 'idb_rei_acf_form_edit_toggle');
        $this->loader->add_action('wp_print_styles', $frontend_forms, 'idb_rei_deregister_admin_styles', 999 );
        endif;
        
        $addAcfFieldGroups = new reiAddACFFields( $this->get_plugin_name(), $this->get_version() );
        $this->loader->add_action('acf/init', $addAcfFieldGroups, 'rei_sites_add_local_field_groups');
        //hide ACF from menu
		$this->loader->add_filter('acf/settings/show_admin', $addAcfFieldGroups, 'my_acf_settings_show_admin');
		
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Rei_Sites_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}