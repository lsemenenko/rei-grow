div.field_type-star_rating_field ul.star-rating {
    font-size: 20px !important;
    margin: 30px 0px;
    vertical-align: middle;
}
a.button.button-small.clear-button {
    background: gold;
    color: black;
    padding: 10px 30px;
    border: 1px solid #0000001c;
}
div.field_type-star_rating_field ul.star-rating li {
    line-height: 1.4 !important;
    padding-right: 15px !important;
}