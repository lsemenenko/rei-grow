<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://idbmedia.com
 * @since      1.0.0
 *
 * @package    Rei_Sites
 * @subpackage Rei_Sites/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Rei_Sites
 * @subpackage Rei_Sites/includes
 * @author     J Hanlon - IDB Media <j@roselane.com>
 */
class Rei_Sites_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
