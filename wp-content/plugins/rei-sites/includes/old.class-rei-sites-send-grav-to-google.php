<?php
/**
 * Class for sending grav forms to google sheets via api post
 *
 * @since 1.0.0
 */

class reiGravFormsToGoogleSheets {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}
    
    
    


function add_to_google_spreadsheet($entry, $form) {


    // This is the web app URL of our Google Script
    if(get_option('rei_sites_app_url')){
    $post_url = get_option('rei_sites_app_url');
            
        } else {
    $post_url = 'https://script.google.com/macros/s/AKfycbzCCe2lq4LRRoC6s-YDTynFGy1gwozpqM-gcl2sZ6ABgiXYv7g/exec'; 
    }

    // Put all the form fields (names and values) in this array
    $body = array(
        'ID' => $entry['id'], 
        'IP' => $entry['ip'], 
        'URL' => $entry['source_url'], 
        'Title' => $form['title'], 
        'Date' => $entry['date_created'], 
        'Address' => rgar($entry, '1'), 
        'Phone' => rgar($entry, '2'), 
        'Email' => rgar($entry, '3'), 
        'Bedrooms' => rgar($entry, '13'), 
        'Bathrooms' => rgar($entry, '14'), 
        'Time Owned' => rgar($entry, '8'), 
        'Condition' => rgar($entry, '9'),
        'Occupied' => rgar($entry, '10'),
        'Sell By Date' => rgar($entry, '11'),
        'Comments' => rgar($entry, '12'),
    );


    // Send the data to Google Spreadsheet via HTTP POST request
    $request = new WP_Http();
    $response = $request->request($post_url, array('method' => 'POST', 'sslverify' => false, 'body' => $body));

}
    
} //end class