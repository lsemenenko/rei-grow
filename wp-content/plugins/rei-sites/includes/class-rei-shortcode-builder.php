<?php
/**
 * Helper class for builder shortcodes
 *
 * @since 1.0.0
 */

class IdbBuilderShortcodes {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
        
         add_shortcode( 'reim_hero_content',array( $this, 'reim_hero_content_shortcode' ));
        add_shortcode( 'reim_hero_content_slideshow',array( $this, 'reim_hero_content_slideshow_shortcode' ));
        add_shortcode( 'reim_hero_content_color',array( $this, 'reim_hero_content_color_shortcode' ));
        add_shortcode( 'reim_hero_content_lead_magnet',array( $this, 'reim_hero_content_lead_magnet_shortcode' ));
        add_shortcode( 'reim_hero_content_lead_magnet_b2',array( $this, 'reim_hero_content_lead_magnet_b2_shortcode' ));
        add_shortcode( 'reim_hero_content_lead_magnet_s2',array( $this, 'reim_hero_content_lead_magnet_s2_shortcode' ));
         add_shortcode( 'reim_main_content',array( $this, 'reim_main_content_shortcode' ));
         add_shortcode( 'reim_add_form_content',array( $this, 'reim_add_form_content_shortcode' ));
        add_shortcode( 'reim_add_form_title',array( $this, 'reim_add_form_title_shortcode' ));
        add_shortcode( 'reim_quicklinks',array( $this, 'reim_quicklinks_shortcode' ));
        add_shortcode( 'reim_projects',array( $this, 'reim_projects_shortcode' ));
         add_shortcode( 'reim_reviews',array( $this, 'reim_reviews_shortcode' ));
         add_shortcode( 'reim_faq',array( $this, 'reim_faq_shortcode' ));
         add_shortcode( 'business_name',array( $this, 'business_name_shortcode' ));
         add_shortcode( 'city_state',array( $this, 'business_city_state_shortcode' ));
        add_shortcode( 'reim_gravform_object',array( $this, 'reim_gravform_object_shortcode' ));
        add_shortcode( 'frontend_editor_bar',array( $this, 'frontend_editor_bar' ));

	}	
    
    
    function frontend_editor_bar(){ 
    if(is_user_logged_in() && is_page() && current_user_can('edit_posts') ){ ?>
      
        <div class='rei-content-edit-bar fade rei-hide-button'> 
            <div class="rei-edit-bar-inner">
                
            <div class="rei-editor-select rei-inner-bar-content fade" style="display:none;">
              <select class="rei-editor-select-fontsize" name="rei-editor-select-fontsize">

                <option value="<p>">Paragraph</option>
                <option value="<h1>">Heading 1</option>
                <option value="<h2>">Heading 2</option>
                <option value="<h3>">Heading 3</option>
                <option value="<h4>">Heading 4</option>
                <option value="<h5>">Heading 5</option>
                <option value="<h6>">Heading 6</option>
                <option value="<h7>">Heading 7</option>
              </select>

            </div>
                
            <div class="rei-editor-buttonbox rei-inner-bar-content">
            <button style="font-weight: 900 !important;" class="rei-editor-style-button" onclick='document.execCommand("bold",false,null);'>Bold</button>
        <button class="rei-editor-style-button" style="font-style: italic" onclick='document.execCommand("italic",false,null);'>Italic</button>
        <button class="rei-editor-style-button" onclick='document.execCommand("justifyLeft",false,null);'><i class="fas fa-align-left"></i></button>
        <button class="rei-editor-style-button" onclick='document.execCommand("justifyCenter",false,null);'><i class="fas fa-align-center"></i></button>
        <button class="rei-editor-style-button" onclick='document.execCommand("justifyRight",false,null);'><i class="fas fa-align-right"></i></button>

                </div>

                </div> 
    </div>
      
    <?php   
    } //end if
    }
    
    
    function reim_gravform_object_shortcode(){
      
        
        return print_r(GFAPI::get_entries( 4 ));
        
    }
    
    
     function reim_hero_content_shortcode(){  
         
        $fields = get_fields(get_the_ID());
        $reiHeroImageID = get_field('field_5d13dfe84c0a8', get_the_ID())['image'];
        $heroImageSize = 'full';
        $reiHeroImageURL = wp_get_attachment_image_url( $reiHeroImageID, $heroImageSize );     
        ob_start(); ?>


<div class="rpp-profile-image-container rpp-profile-image" data-id="<?php echo $reiHeroImageID; ?>" id="rei-hero-image" style="background: linear-gradient(0deg,rgba(0,0,0,0.2),rgba(0,0,0,0.2)), url(<?php echo $reiHeroImageURL ?>); width:100%; background-size: cover; background-position: center center;
    background-repeat: no-repeat;">
    <div class="reim-hero-content"><h1 class="rei-hero-title" contenteditable="false">
        <?php echo $fields['reim_hero']['title']; ?></h1>
        <h3 class="rei-hero-subtitle" contenteditable="false"><?php echo $fields['reim_hero']['subtitle'];?></h3>  </div><?php if ( current_user_can('manage_options') && is_user_logged_in() ){ ?><div class="fade rpp-button-change-image rei-hide-button" id="rpp-profile-image"> <i style="margin-right:5px" class="fa fa-pencil"></i> Edit Image</div> <?php } ?></div>  


   <?php 
        //echo '<div style="padding:20px;">'.print_r($field_objects).'</div>';
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
         
 } // end reim_hero_content_shortcode
    
    
    
    
    function reim_hero_content_slideshow_shortcode(){  
         
        $fields = get_fields(get_the_ID());
        $reiHeroImageID = get_field('field_5d13dfe84c0a8', get_the_ID())['image'];
        $heroImageSize = 'full';
        $reiHeroImageURL = wp_get_attachment_image_url( $reiHeroImageID, $heroImageSize );
        ob_start(); ?>

<style>

video {
  position: absolute;
  top: 50%;
  left: 50%;
  min-width: 100%;
  min-height: 100%;
  width: auto;
  height: auto;
  z-index: 0;
  -ms-transform: translateX(-50%) translateY(-50%);
  -moz-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translateX(-50%) translateY(-50%);
  transform: translateX(-50%) translateY(-50%);
    z-index: 0;
}

.container {
  position: relative;
  z-index: 2;
}

.overlay {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: black;
  opacity: 0.2;
  z-index: 1;
}

@media (pointer: coarse) and (hover: none) {
  header {
    background: url('https://source.unsplash.com/XT5OInaElMw/1600x900') black no-repeat center center scroll;
  }
  header video {
    display: none;
  }
}</style>


    <div class="rei-hero-slideshow-container">
    	<div class="other-content"><h1 class="rei-hero-title" contenteditable="false"><?php echo $fields['reim_hero']['title']; ?></h1><h3 class="rei-hero-subtitle" contenteditable="false"><?php echo $fields['reim_hero']['subtitle'];?></h3></div>

    	<div class="overlay"></div>
        <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="<?php echo get_field('reim_hero_background_video'); ?>" type="video/mp4">
  </video>
        
                
        <div class="slide show" style="background: linear-gradient(0deg,rgba(0,0,0,0.2),rgba(0,0,0,0.2)), url(<?php echo get_field('reim_hero_slider_image_1'); ?>); width:100%; background-size: cover; background-position: center center;"></div>
    	<div class="slide" style="background: linear-gradient(0deg,rgba(0,0,0,0.2),rgba(0,0,0,0.2)), url(<?php echo get_field('reim_hero_slider_image_2'); ?>); width:100%; background-size: cover; background-position: center center;"></div>

    </div>

        
    <script>

    function cycleBackgrounds() {
    	var index = 0;

    	$imageEls = $('.rei-hero-slideshow-container .slide'); // Get the images to be cycled.

    	setInterval(function () {
    		// Get the next index.  If at end, restart to the beginning.
    		index = index + 1 < $imageEls.length ? index + 1 : 0;
    		// Show the next image.
    		$imageEls.eq(index).addClass('show');
    		// Hide the previous image.
    		$imageEls.eq(index - 1).removeClass('show');

    	}, 6000);
    };

    // Document Ready.
    $(function () {
    	cycleBackgrounds();
    });

        </script>



   <?php 
        //echo '<div style="padding:20px;">'.print_r($field_objects).'</div>';
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
         
 } // end reim_hero_content_slideshow_shortcode
    
    
    
    
    
    function reim_hero_content_color_shortcode(){  
         
        $fields = get_fields(get_the_ID());
        $reiHeroImageID = get_field('field_5d13dfe84c0a8', get_the_ID())['image'];
        $heroImageSize = 'full';
        $reiHeroImageURL = wp_get_attachment_image_url( $reiHeroImageID, $heroImageSize );     
        ob_start(); ?>


<div class="container-fluid rpp-profile-image-container rpp-profile-image" data-id="<?php echo $reiHeroImageID; ?>" id="rei-hero-image" style=" width:100%; background-size: cover; background-position: center center;
    background-repeat: no-repeat;">
    <div class="container-fluid reim-hero-content"><h1 class="rei-hero-title" contenteditable="false">
        <?php echo $fields['reim_hero']['title']; ?></h1>
        <h3 class="rei-hero-subtitle" contenteditable="false"><?php echo $fields['reim_hero']['subtitle'];?></h3>  </div><?php if ( current_user_can('manage_options') && is_user_logged_in() ){ ?><div class="fade rpp-button-change-image rei-hide-button" id="rpp-profile-image"> <i style="margin-right:5px" class="fa fa-pencil"></i> Edit Image</div> <?php } ?></div>  


   <?php 

    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
         
 } // end profile image shortcode
    
    
       function reim_hero_content_lead_magnet_shortcode(){  
        $fields = get_fields(get_the_ID());
        $reiHeroImageID = get_field('field_5d13dfe84c0a8', get_the_ID())['image'];
        $heroImageSize = 'full';
        $reiHeroImageURL = wp_get_attachment_image_url( $reiHeroImageID, $heroImageSize );         
        ob_start(); ?>


<div class="container-fluid rpp-profile-image-container rpp-profile-image" data-id="<?php echo $reiHeroImageID; ?>" id="rei-hero-image" style=" width:100%; background-size: cover; background-position: center center;
    background-repeat: no-repeat;">
    <div class="row rei-hero-row">
        <div class="col-lg-8 m-auto text-white">
        <div class="reim-hero-content"><h1 class="rei-hero-title" contenteditable="false">
        <?php echo $fields['reim_hero']['title']; ?></h1>
            <h3 class="rei-hero-subtitle" contenteditable="false"><?php echo $fields['reim_hero']['subtitle'];?></h3>
            <div class="rei-font-styling-on rei-hero-content" contenteditable="false"><?php echo get_field('reim_hero_content') ?></div>
        </div>
            <span class="reim_magnet_arrow"><h3>Just Complete This Quick Form!</h3><img class="reim-arrow" style="width:175px; height:75px" src="<?php echo get_stylesheet_directory_uri();?>/assets/lead-magnet-arrow.png">
            <img class="reim-arrow-mobile" style="width:100px; height:75px" src="<?php echo get_stylesheet_directory_uri();?>/assets/lead-magnet-arrow-mobile.png">
            </span>
        
        </div>
        <?php if ( current_user_can('manage_options') && is_user_logged_in() ){ ?><div class="fade rpp-button-change-image rei-hide-button" id="rpp-profile-image"> <i style="margin-right:5px" class="fa fa-pencil"></i> Edit Image</div> <?php } ?>
        <div class="col-lg-4"><div class="p-5 rei_lead_magnet_bg">
						<h3 class="rei-lead-magnet-title"><?php echo get_field('rei_b1_lead_magnet_options_title', 'options'); ?></h3>
						<p class="rei-lead-magnet-description"><?php echo get_field('rei_b1_lead_magnet_options_paragraph', 'options'); ?></p><?php echo do_shortcode('[gravityform id=3 title=false]
						'); ?>
                        <p class="rei-lead-magnet-privacy">We respect your privacy. See our <a href="/privacy-policy">privacy policy</a>.</p>
					</div></div></div></div>


   <?php 

    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
         
 } // end hero content lead magnet shortcode
    
    
    
    function reim_hero_content_lead_magnet_b2_shortcode(){  
         
        $fields = get_fields(get_the_ID());
        $reiHeroImageID = get_field('field_5d13dfe84c0a8', get_the_ID())['image'];
        $heroImageSize = 'full';
        $reiHeroImageURL = wp_get_attachment_image_url( $reiHeroImageID, $heroImageSize );    
        ob_start(); ?>


<div class="container-fluid rpp-profile-image-container rpp-profile-image" data-id="<?php echo $reiHeroImageID; ?>" id="rei-hero-image" style=" width:100%; background-size: cover; background-position: center center;
    background-repeat: no-repeat;">
    <div class="row rei-hero-row">
        <div class="col-lg-8 m-auto text-white">
        <div class="reim-hero-content"><h1 class="rei-hero-title" contenteditable="false">
        <?php echo $fields['reim_hero']['title']; ?></h1>
            <h3 class="rei-hero-subtitle" contenteditable="false"><?php echo $fields['reim_hero']['subtitle'];?></h3>
            <div class="rei-font-styling-on rei-hero-content" contenteditable="false"><?php echo get_field('reim_hero_content') ?></div>
        </div>
            <span class="reim_magnet_arrow"><h3>Just Complete This Quick Form!</h3><img class="reim-arrow" style="width:175px; height:75px" src="<?php echo get_stylesheet_directory_uri();?>/assets/lead-magnet-arrow.png">
            <img class="reim-arrow-mobile" style="width:100px; height:75px" src="<?php echo get_stylesheet_directory_uri();?>/assets/lead-magnet-arrow-mobile.png">
            </span>
        
        </div>
        <?php if ( current_user_can('manage_options') && is_user_logged_in() ){ ?><div class="fade rpp-button-change-image rei-hide-button" id="rpp-profile-image"> <i style="margin-right:5px" class="fa fa-pencil"></i> Edit Image</div> <?php } ?>
        <div class="col-lg-4"><div class="p-5 rei_lead_magnet_bg">
						<h3 class="rei-lead-magnet-title"><?php echo get_field('rei_b2_lead_magnet_options_title', 'options'); ?></h3>
						<p class="rei-lead-magnet-description"><?php echo get_field('rei_b2_lead_magnet_options_paragraph', 'options'); ?></p><?php echo do_shortcode('[gravityform id=3 title=false]
						'); ?>
                        <p class="rei-lead-magnet-privacy">We respect your privacy. See our <a href="/privacy-policy">privacy policy</a>.</p>
					</div></div></div></div>


   <?php 
      
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
         
 } // end hero content lead magnet b2 shortcode
    
    
    
    function reim_hero_content_lead_magnet_s2_shortcode(){  
         
        $fields = get_fields(get_the_ID());
        $reiHeroImageID = get_field('field_5d13dfe84c0a8', get_the_ID())['image'];
        $heroImageSize = 'full';
        $reiHeroImageURL = wp_get_attachment_image_url( $reiHeroImageID, $heroImageSize );
        ob_start(); ?>


<div class="container-fluid rpp-profile-image-container rpp-profile-image" data-id="<?php echo $reiHeroImageID; ?>" id="rei-hero-image" style=" width:100%; background-size: cover; background-position: center center;
    background-repeat: no-repeat;">
    <div class="row rei-hero-row">
        <div class="col-lg-8 m-auto text-white">
        <div class="reim-hero-content"><h1 class="rei-hero-title" contenteditable="false">
        <?php echo $fields['reim_hero']['title']; ?></h1>
            <h3 class="rei-hero-subtitle" contenteditable="false"><?php echo $fields['reim_hero']['subtitle'];?></h3>
            <div class="rei-font-styling-on rei-hero-content" contenteditable="false"><?php echo get_field('reim_hero_content') ?></div>
        </div>
            <span class="reim_magnet_arrow"><h3>Just Complete This Quick Form!</h3><img class="reim-arrow" style="width:175px; height:75px" src="<?php echo get_stylesheet_directory_uri();?>/assets/lead-magnet-arrow.png">
            <img class="reim-arrow-mobile" style="width:100px; height:75px" src="<?php echo get_stylesheet_directory_uri();?>/assets/lead-magnet-arrow-mobile.png">
            </span>
        
        </div>
        <?php if ( current_user_can('manage_options') && is_user_logged_in() ){ ?><div class="fade rpp-button-change-image rei-hide-button" id="rpp-profile-image"> <i style="margin-right:5px" class="fa fa-pencil"></i> Edit Image</div> <?php } ?>
        <div class="col-lg-4"><div class="p-5 rei_lead_magnet_bg">
						<h3 class="rei-lead-magnet-title"><?php echo get_field('rei_s2_lead_magnet_options_title', 'options'); ?></h3>
						<p class="rei-lead-magnet-description"><?php echo get_field('rei_s2_lead_magnet_options_paragraph', 'options'); ?></p><?php echo do_shortcode('[gravityform id=3 title=false]
						'); ?>
                        <p class="rei-lead-magnet-privacy">We respect your privacy. See our <a href="/privacy-policy">privacy policy</a>.</p>
					</div></div></div></div>


   <?php 
        //echo '<div style="padding:20px;">'.print_r($field_objects).'</div>';
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
         
 } // end hero content lead magnet s2 shortcode
    
    
    
    
    
function reim_main_content_shortcode(){  ob_start();?>


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<style>

    
.rei-hide-button {
    display: none;
}    
    
.save-icon{
  position:relative;
  background:white;
  height:40px;
  width:35px;
  display:block;
  padding-top:10px;
  -moz-border-radius:3px;
  border-radius:3px;
}

.save-icon:before{
   content:"";
   position:absolute;
   top:0;
   right:0;
   border-width:0 10px 10px 0;
   border-style:solid;
   border-color:#a4a6a7 #3c4145;
}

.save-icon:after{
  content:"✓";
  color:green;
  font-size:30px;
  position:absolute;
  top:15%;left:20%;
  -webkit-transform:scale(0);
  -moz-transform:scale(0);
  transform:scale(0);
  
  -webkit-animation: pop 0.5s 3s forwards;
     -moz-animation: pop 0.5s 3s forwards;
          animation: pop 0.5s 3s forwards;  
}



.loader{
  background:#e2e2e2;
  width:80%;
  height:5px;
  display:block;
  margin:3px auto;
  
  position:relative;
  overflow:hidden;
  
  
  -webkit-animation:fade-loaders 0.2s 3s forwards;
     -moz-animation:fade-loaders 0.2s 3s forwards;
          animation:fade-loaders 0.2s 3s forwards;
}


.loader:after{
  content: "";
  background:#2c3033;
  width:0;
  height:5px;
  position:absolute;
  top:0;left:0;
}

.loader:first-child:after{
  -webkit-animation: loader 0.4s 1s forwards;
     -moz-animation: loader 0.4s 1s forwards;
          animation: loader 0.4s 1s forwards;
}


.loader:nth-child(2n):after{
  -webkit-animation: loader 0.4s 1.5s forwards;
     -moz-animation: loader 0.4s 1.5s forwards;
          animation: loader 0.4s 1.5s forwards;
}

.loader:nth-child(3n):after{
  -webkit-animation: loader 0.4s 2s forwards;
     -moz-animation: loader 0.4s 2s forwards;
          animation: loader 0.4s 2s forwards;
}


@-webkit-keyframes loader {0%{ width: 0%; }100% { width: 100%; }}
   @-moz-keyframes loader {0%{ width: 0%; }100% { width: 100%; }}
        @keyframes loader {0%{ width: 0%; }100% { width: 100%; }}

@-webkit-keyframes pop {
  0%   { -webkit-transform: scale(0); }
  100% { -webkit-transform: scale(1); }
}
@-moz-keyframes pop {
  0%   { -moz-transform: scale(0); }
  100% { -moz-transform: scale(1); }
}
@keyframes pop {
  0%   { transform: scale(0); }
  100% { transform: scale(1); }
}

@-webkit-keyframes fade-loaders{
  0%   { opactity:1; }
  100% { opacity:0; }
}
@-moz-keyframes fade-loaders{
  0%   { opactity:1; }
  100% { opacity:0; }
}
@keyframes fade-loaders{
  0%   { opactity:1; }
  100% { opacity:0; }
}
    #followTab {
        display: -webkit-box;
    }
     @keyframes rotate-loading {
            0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
            100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
        }

        @-moz-keyframes rotate-loading {
            0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
            100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
        }

        @-webkit-keyframes rotate-loading {
            0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
            100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
        }

        @-o-keyframes rotate-loading {
            0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
            100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
        }

        @keyframes rotate-loading {
            0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
            100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
        }

        @-moz-keyframes rotate-loading {
            0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
            100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
        }

        @-webkit-keyframes rotate-loading {
            0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
            100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
        }

        @-o-keyframes rotate-loading {
            0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
            100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
        }

        @keyframes loading-text-opacity {
            0%  {opacity: 0}
            20% {opacity: 0}
            50% {opacity: 1}
            100%{opacity: 0}
        }

        @-moz-keyframes loading-text-opacity {
            0%  {opacity: 0}
            20% {opacity: 0}
            50% {opacity: 1}
            100%{opacity: 0}
        }

        @-webkit-keyframes loading-text-opacity {
            0%  {opacity: 0}
            20% {opacity: 0}
            50% {opacity: 1}
            100%{opacity: 0}
        }

        @-o-keyframes loading-text-opacity {
            0%  {opacity: 0}
            20% {opacity: 0}
            50% {opacity: 1}
            100%{opacity: 0}
        }
        .loading-container,
        .loading {
            height: 25px;
            position: relative;
            width: 25px;
            border-radius: 100%;
        }


        .loading-container { margin: 3px auto }

        .loading {
            border: 2px solid transparent;
            border-color: transparent #fff transparent #FFF;
            -moz-animation: rotate-loading 1.5s linear 0s infinite normal;
            -moz-transform-origin: 50% 50%;
            -o-animation: rotate-loading 1.5s linear 0s infinite normal;
            -o-transform-origin: 50% 50%;
            -webkit-animation: rotate-loading 1.5s linear 0s infinite normal;
            -webkit-transform-origin: 50% 50%;
            animation: rotate-loading 1.5s linear 0s infinite normal;
            transform-origin: 50% 50%;
        }

        .loading-container:hover .loading {
            border-color: transparent #E45635 transparent #E45635;
        }
        .loading-container:hover .loading,
        .loading-container .loading {
            -webkit-transition: all 0.5s ease-in-out;
            -moz-transition: all 0.5s ease-in-out;
            -ms-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
        }
.jconfirm .jconfirm-holder {
    max-height: 100%;
    padding: 50px 0;
    max-width: 400px;
    margin: auto;
}
        /* The tab itself */

#followTab {


  
  /* Give the tab width and padding */

      padding: 7px 10px;
    max-width: 170px;
  
  /* Add the curved white border */
  border: 3px solid #fff;
  border-left: none;
  -moz-border-radius: 0px 10px 10px 0px;
  -webkit-border-radius: 0px 10px 10px 0px;
  border-radius: 0px 10px 10px 0px;
  
  /* Add the drop shadow */
  -moz-box-shadow: 0 0 7px rgba(0, 0, 0, .6);
  -webkit-box-shadow: 0 0 7px rgba(0, 0, 0, .6);
  box-shadow: 0 0 7px rgba(0, 0, 0, .6);
  
  /* Add the semitransparent gradient background */

  background: #3e4757a8;
  background: linear-gradient(top, rgba(243, 52, 8, .75), rgba(239, 91, 10, .75));
  filter: progid:DXImageTransform.Microsoft.Gradient( startColorStr='#c0f33408', endColorStr='#c0ef5b0a', GradientType=0 );
}
    
    .edit-bar{
          position: fixed;
  z-index: 1;
  left: -125px;
  bottom: 80px;
       width: 160px; 

                    -webkit-transition: all 0.5s ease-in-out;
            -moz-transition: all 0.5s ease-in-out;
            -ms-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
    }
    .opened-edit-bar{
          position: fixed;
  z-index: 1;
  left: 0px;
  bottom: 80px;
                    -webkit-transition: all 0.5s ease-in-out;
            -moz-transition: all 0.5s ease-in-out;
            -ms-transition: all 0.5s ease-in-out;
            -o-transition: all 0.5s ease-in-out;
            transition: all 0.5s ease-in-out;
    }
.edit-bar-btn {
    background: white;
    border: 3px solid #696969b5;
    border-radius: 60px;
    width: 47px;
    display: block;
    position: relative;
    left: 8px;
    height: 47px;
    line-height: 40px;
    text-align: center;
    font-size: 13px;
    cursor: pointer;
    font-weight: 500;
    color: #272727;
    font-weight: 700;
}
    button#submit-2 {
    position: relative;
        text-transform: uppercase;
    background: white;
    border: 3px solid #53d860;
    border-radius: 60px;
    line-height: 40px;
    text-align: center;
    font-size: 13px;
    cursor: pointer;
    font-weight: 700;
    color: #272727;
    padding:  0 30px 0 30px;
    margin: 0 10px 0 10px;
}
    /*
      [contenteditable="true"]{
background: aliceblue;
          color: #313131 !important;
        
          
}   
    */
    [contenteditable="true"] {
    background: #f0f8ff26;
    border: 2px dotted #909090;
    border-radius: 4px;    }
    </style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>


<div class="rpp-profile-container">
    <?php if ( current_user_can('manage_options') && is_user_logged_in()){ ?>
    
    
    <div id="followTab" class="edit-bar">
        <input type="hidden"; id="rei-page-id" class="<?php echo get_the_ID(); ?>" >
<button id="submit-2" class="button idb-edit-resume" style="astra_header_before">SAVE</button>
        <div class="edit-bar-btn">EDIT</div>
</div>
    <?php } //endif logged_in()
    
       $fields = get_fields(get_the_ID()); ?>
    
         <div class="rpp-profile-text-container" >
             
             
             <?php if(isset($fields['reim_main']['title'])){ ?>
        <h2 class="rei-content-title" contenteditable="false"><?php echo $fields['reim_main']['title'];?></h2>
             <?php } elseif (isset($fields['reim_home_main']['title'])){ ?>
        <h2 class="rei-content-title" contenteditable="false"><?php echo $fields['reim_home_main']['title'];?></h2>
             <?php } // endif 
                
                                       
                if(isset($fields['reim_main']['content'])){ ?>
        <div id="rei-main-content" class="rei-main-content rei-font-styling-on" contenteditable="false"><?php echo $fields['reim_main']['content']; ?></div>
           <?php  } 
                                       
         elseif (isset($fields['reim_home_main']['content'])){ ?>
        <div id="rei-main-content" class="rei-main-content rei-font-styling-on" contenteditable="false"><?php echo $fields['reim_home_main']['content']; ?></div>
           <?php  } 
                                       
         elseif (isset($fields['reim_default_main']['content'])){ ?>
        <div id="rei-main-content" class="rei-main-content rei-font-styling-on" contenteditable="false"><?php echo $fields['reim_default_main']['content']; ?></div>
           <?php  } ?>
            
             
     
    </div>
</div>

        
   <?php 
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
 } // end profile shortcode
    
    
    
  function formatPhoneNumber($phoneNumber) {
    $phoneNumber = preg_replace('/[^0-9]/','',$phoneNumber);

    if(strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree.'-'.$lastFour;
    }

    return $phoneNumber;
}
    

function reim_add_form_title_shortcode(){  
    
    $fields = get_fields(get_the_ID());

         ob_start();?>


   <h4 id="rei-add-form-title" class="rei-font-styling-on rei-add-form-title" contenteditable="false"><?php echo get_field('rei_add_form_title'); ?></h4>
        
        
   <?php 
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
} // end Form Title

function reim_add_form_content_shortcode(){  
    
    $fields = get_fields(get_the_ID());

         ob_start();?>


 <div id="rei-add-form-content" class="rei-font-styling-on rei-add-form-content" contenteditable="false"><?php echo get_field('rei_add_form_content'); ?></div>
        
        
   <?php 
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
} // end Main Content 2    

    
function reim_quicklinks_shortcode(){  
    
    $fields = get_fields(get_the_ID());

         ob_start();?>


 <div class="section bg-light">
            <div class="container rei-main-container">
                <div class="row pb-4"> <div class="col text-center"><h2 contenteditable="false" class="rei-quicklinks-title"><?php echo $fields['rei_quicklinks']['title']; ?></h2><div class="rei-quicklinks-content" contenteditable="false"><?php echo $fields['rei_quicklinks']['content']; ?> </div>
                </div>
                                </div>
                            <div class="row">
            
                <?php if( get_field('rei_quicklinks_1_status') == 'show'): ?>
                <div class="col quicklink-container">
            <div class="container-blocks image-container">
  <a href="<?php echo get_field('rei_quicklinks_link_1'); ?>"> <img src="<?php echo get_field('rei_quicklinks_image_1'); ?>" style="width:100%;">
      <div class="after"></div>
  <div class="centered text-white"><h2><?php echo get_field('rei_quicklinks_title_1'); ?></h2></div></a>
</div>
            </div>
                <?php endif; ?>
                
                <?php if( get_field('rei_quicklinks_2_status') == 'show'): ?>
                <div class="col quicklink-container">
            <div class="container-blocks image-container">
   <a href="<?php echo get_field('rei_quicklinks_link_2'); ?>"> <img src="<?php echo get_field('rei_quicklinks_image_2'); ?>" style="width:100%;">
             <div class="after"></div>
  <div class="centered text-white"><h2><?php echo get_field('rei_quicklinks_title_2'); ?></h2></div></a>
</div>
                    </div>
                    <?php endif; ?>
                    
                    <?php if( get_field('rei_quicklinks_3_status') == 'show'): ?>
            <div class="col quicklink-container">
            <div class="container-blocks image-container">
  <a href="<?php echo get_field('rei_quicklinks_link_3'); ?>"> <img href="#" src="<?php echo get_field('rei_quicklinks_image_3'); ?>" style="width:100%;">
                   <div class="after"></div>
  <div class="centered text-white"><h2><?php echo get_field('rei_quicklinks_title_3'); ?></h2></div></a>
</div>
            </div>
               <?php endif; ?> 
                
                
                </div></div></div>
        
        
   <?php 
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
} // end Quicklinks Shortcode   
    

    function reim_projects_shortcode(){  
    
    $fields = get_fields(get_the_ID());

         ob_start();?>


 <div class="rei-projects-container" >
    <h4 class="rei-projects-title" contenteditable="false"><?php echo $fields['rei_home_projects']['title']; ?></h4>
       <h2 class="rei-projects-subtitle" contenteditable="false"><?php echo $fields['rei_home_projects']['subtitle']; ?></h2>
       <hr/ class="rei-projects-content-divider">
     <div class="rei-projects-content rei-font-styling-on" contenteditable="false"><?php echo $fields['rei_home_projects']['content']; ?></div>
              </div>
        
        
   <?php 
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
} // end Projects Shortcode
    
    
function reim_reviews_shortcode(){  
    
    $fields = get_fields(get_the_ID());

         ob_start();?>


 <div class="reim-reviews-container" >
    <h4 class="reim-reviews-title" contenteditable="false"><?php echo $fields['reim_home_reviews']['title']; ?></h4>
       <h2 class="reim-reviews-subtitle" contenteditable="false"><?php echo $fields['reim_home_reviews']['subtitle']; ?></h2>
       <hr class="reim-reviews-content-divider" />
     <div class="reim-reviews-content rei-font-styling-on" contenteditable="false"><?php echo $fields['reim_home_reviews']['content']; ?></div>
              </div>
        
        
   <?php 
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
} // end Reviews Shortcode
    
    
    function business_name_shortcode(){  
    
         ob_start();?>

<?php echo get_field('rei_theme_options_business_info_legal_name','options'); ?>      
        
   <?php 
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
} // end Business Name Shortcode
    
    
    function business_city_state_shortcode(){  
    
         ob_start();?>

<?php echo get_field('rei_theme_options_business_info_address_city','options'); ?>, <?php echo get_field('rei_theme_options_business_info_address_state','options'); ?>      
    
        
   <?php 
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
} // end Business Name Shortcode
  
    
    function reim_faq_shortcode(){  
        
        $fields = get_fields(get_the_ID());
        
         ob_start();?>


 <div class="reim-faq-container" >
    <h2 class="reim-faq-title" contenteditable="false"><?php echo $fields['reim_home_faq']['title']; ?></h2>
              </div>
<div class="reim-faq-questions-outer-container" >
    <div class="reim-faq-question-container" >
    <h4 class="reim-faq-question" contenteditable="false"><?php echo $fields['reim_home_faq']['question_1']; ?></h4>
        <div class="reim-faq-answer" contenteditable="false"><?php echo $fields['reim_home_faq']['answer_1']; ?>
        </div>
        <button class="reim_faq_button"
                onclick="window.location.href = '/faq';">Read More ></button>
    </div>
    <div class="reim-faq-question-container" >
    <h4 class="reim-faq-question" contenteditable="false"><?php echo $fields['reim_home_faq']['question_2']; ?></h4>
        <div class="reim-faq-answer" contenteditable="false"><?php echo $fields['reim_home_faq']['answer_2']; ?>
    </div>
        <button class="reim_faq_button" onclick="window.location.href = '/faq';">Read More ></button>
    </div>
    <div class="reim-faq-question-container-last" >
    <h4 class="reim-faq-question" contenteditable="false"><?php echo $fields['reim_home_faq']['question_3']; ?></h4>
        <div class="reim-faq-answer" contenteditable="false"><?php echo $fields['reim_home_faq']['answer_3']; ?>
    </div>
    <button class="reim_faq_button" onclick="window.location.href = '/faq';">Read More ></button>
    </div>
    </div>
        
        
   <?php 
    $profile = ob_get_contents();
    ob_end_clean();
    return $profile;
} // end FAQ Shortcode 1
    
    
} // end class