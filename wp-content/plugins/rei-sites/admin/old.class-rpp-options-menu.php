<?php
/**
 * RPP_OptionsMenu Class Init
 *
 * Handles the infrastructure of this part of the plugin
 *
 * @author      J Hanlon
 * @category    Admin
 * @package     RPP/Menu
 * @version     0.0.1
*/

if (!defined('ABSPATH')) {
  exit;
}

class RPP_OptionsMenu {

  /**
   * Holds the instance
   * 
   * @return RPP_OptionsMenu
   */
  static $instance;

  /**
   * Returns the one and only instance of this class
   *
   */
  public static function get_instance() {

    if (!isset(self::$instance)) {

      self::$instance = new self();

    } // end if;

    return self::$instance;

  } // end get_instance;

  /**
   * Instantiates the class
   */
  public function __construct() {

      add_action('admin_menu', array( $this, 'idb_resumepagepro_options_menu'));
      
  } // end construct;

    

        //add menu page    
     function idb_resumepagepro_options_menu(){
 	
 	

         // add parent
	$resume_options_menu = acf_add_options_page(array(
		'page_title' 	=> 'Resume Form',
		'menu_title' 	=> 'Resume Options',
		'redirect' 		=> true
	));

        
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title' 	=> 'Site Options',
        'menu_slug' => 'theme-options',
        'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
	
		// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title' 	=> 'Theme Options',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
             // add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Test Page',
		'menu_title' 	=> 'Test Page',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Profile',
		'menu_title' 	=> 'Profile',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
                     	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Contact Info',
		'menu_title' 	=> 'Contact Info',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
         
         	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Experience',
		'menu_title' 	=> 'Experience',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
         
             	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Education',
		'menu_title' 	=> 'Education',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
                      
         	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Skills',
		'menu_title' 	=> 'Skills',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
         
         	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Awards',
		'menu_title' 	=> 'Awards',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
                  
         	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Interests',
		'menu_title' 	=> 'Interests',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
              	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'References',
		'menu_title' 	=> 'References',
		'parent_slug' 	=> $resume_options_menu['menu_slug'],
	));
} 
    
} // end class

new RPP_OptionsMenu();

