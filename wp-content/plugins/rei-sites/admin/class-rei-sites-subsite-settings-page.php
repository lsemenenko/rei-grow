<?php
class Rei_Sites_SubsiteSettingsPage {

    	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
    

	}
    


    function register_profile_page(){
        
        $pagetitle='My Profile';
        $parentmenu='rei-profile';   
        $menutitle='My Profile';
            $cap='manage_options';
            $slug='rei-profile'; 
            $icon='dashicons-money';
            $pos= null;
        
                    // add options page
        global $rei_topmenu;
        $rei_topmenu =
        add_menu_page(
        'Settings',
        'Settings',
        $cap,
        $slug,
        array($this,'profile_page_callback'),
        $icon,
        $pos
    );
        
       global $rei_mainprofilepage;  
        
       $rei_mainprofilepage = add_submenu_page(
                    $parentmenu,
                    $pagetitle,
                    $menutitle,
                    $cap,
                    $slug,
                    array($this,'profile_page_callback'),
                    $icon,
                    $pos
                      
                     );
        

        
        
    }
    
    public function profile_page_callback(){
       /* Get user info. */
global $current_user, $wp_roles;
        
        $profileuser = wp_get_current_user();
                    if (empty(get_option('idb_display_name')) ){
               $idb_display_name_1 = $profileuser->display_name;
           } else {
               $idb_display_name_1 = get_option('idb_display_name');
           }  
                   if (empty(get_option('idb_nickname')) ){
               $idb_nickname_1 = $profileuser->nickname;
           } else {
               $idb_nickname_1 = get_option('idb_nickname');
           }
                ?>
   
    <html lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="//www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins">
    
<!-- !PAGE CONTENT! -->
<div class="w3-main idb-container" style="margin-left:40px;margin-right:40px">
    
<div class="idb_denta_profile_container">
        <h1 class="w3-jumbo w3-text-white"><b>My Profile</b></h1>
    <h1 class="w3-xxxlarge w3-text-red" style="display:inline-block;"><b>Take Control.</b></h1>
    <hr style="width:50px;border:5px solid red" class="w3-round">

            
    <?php 


$error = array();    
/* If profile was saved, update profile. */
if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

    /* Update user password. */
    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
        if ( $_POST['pass1'] == $_POST['pass2'] )
            wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
        else
            $error[] = __('The passwords you entered do not match.  Your password was not updated.', 'profile');
    }

    /* Update user information. */
   
    if ( !empty( $_POST['email'] ) ){
        if (!is_email(esc_attr( $_POST['email'] )))
            $error[] = __('The Email you entered is not valid.  please try again.', 'profile');
        else if( email_exists( esc_attr($_POST['email'])  && esc_attr($_POST['email']) !== $current_user->ID))
            $error[] = __('This email is already being used. Please try a different one.', 'profile');
        else{
            wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
        }
    }

    if ( !empty( $_POST['first-name'] ) )
        update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
    if ( !empty( $_POST['last-name'] ) )
        update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
    if ( !empty( $_POST['nickname'] ) )
        update_user_meta($current_user->ID, 'nickname', esc_attr( $_POST['nickname'] ) );
    if ( !empty( $_POST['display_name'] ) )
        wp_update_user(array ('ID' => $current_user->ID, 'display_name' => esc_attr( $_POST['display_name'] )));
   if ( !empty( $_POST['blogname'] ) )
        update_option('blogname', esc_attr( $_POST['blogname'] ) );
    if ( !empty( $_POST['blogdescription'] ) )
        update_option('blogdescription', esc_attr( $_POST['blogdescription'] ) );
    if ( !empty( $_POST['idb_cto_ss_1'] ) )
        update_option('idb_cto_ss_1', $_POST['idb_cto_ss_1'] );
    if ( empty( $_POST['idb_cto_ss_1'] ) )
        update_option('idb_cto_ss_1', null );
    
                                                    
   //$user_id = $profileuser->ID;

    /* Redirect so the page will show updated info.*/

    if ( count($error) == 0 ) {
        //action hook for plugins and extra fields saving
        do_action('edit_user_profile_update', $current_user->ID);
        wp_redirect( '/wp-admin/admin.php?page=rei-profile' );
        exit;
    }
      if ( count($error) > 0 ) echo '<p class="rei-profile-error">' . implode("<br />", $error) . '</p>';
} ?>
    
                <form method="post" id="adduser" action="<?php the_permalink(); ?>">
          
                    
    <?php settings_fields( 'idb-site-settings' );
  
if (! get_bloginfo( 'name' )){
               $idb_blogname_1 = null;
           } else {
               $idb_blogname_1 = get_bloginfo( 'name' );
           } 
                                           
if (! get_bloginfo( 'description' )){
     $idb_blog_description = null;
}else {
     $idb_blog_description = get_bloginfo( 'description' );
}

  if (isset($_GET['updated'])): ?>
<div id="message" class="updated notice is-dismissible"><p><?php _e('Menu Pro options saved.') ?></p></div>
  <?php endif; ?>  
                <p class="form-username">
                <label for="first-name"><?php _e('First Name', 'profile'); ?></label>
                    <input class="text-input" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
                </p><!-- .form-username -->
                <p class="form-username">
                <label for="last-name"><?php _e('Last Name', 'profile'); ?></label>
                    <input class="text-input" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
                </p><!-- .form-username -->
                   
	<label for="nickname"><?php _e( 'Nickname *' ); ?> <span class="description"></span></label>
	<input type="text" name="nickname" id="nickname" value="<?php the_author_meta( 'nickname', $current_user->ID ); ?>" class="regular-text" />
<p>
<label for="display_name"><?php _e( 'Display Name' ); ?></label>
	
		<select name="display_name" id="display_name">
		<?php
			$public_display                     = array();
			$public_display['display_nickname'] = $profileuser->nickname;
			$public_display['display_username'] = $profileuser->user_login;

		if ( ! empty( $profileuser->first_name ) ) {
			$public_display['display_firstname'] = $profileuser->first_name;
		}

		if ( ! empty( $profileuser->last_name ) ) {
			$public_display['display_lastname'] = $profileuser->last_name;
		}

		if ( ! empty( $profileuser->first_name ) && ! empty( $profileuser->last_name ) ) {
			$public_display['display_firstlast'] = $profileuser->first_name . ' ' . $profileuser->last_name;
			$public_display['display_lastfirst'] = $profileuser->last_name . ' ' . $profileuser->first_name;
		}

		if ( ! in_array( $profileuser->display_name, $public_display ) ) { // Only add this if it isn't duplicated elsewhere
			$public_display = array( 'display_displayname' => $profileuser->display_name ) + $public_display;
		}

			$public_display = array_map( 'trim', $public_display );
			$public_display = array_unique( $public_display );

		foreach ( $public_display as $id => $item ) {
			?>
		<option <?php selected( $profileuser->display_name, $item ); ?>><?php echo $item; ?></option>
			<?php
		}
		?>
		</select>
		</p>
	
                    <p class="form-email">
                        <label for="email"><?php _e('Account Email *', 'profile'); ?></label>
                        <input class="text-input" name="email" type="email" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
                    </p><!-- .form-email -->
                    
                    <p class="form-password">
                        <label for="pass1"><?php _e('Password *', 'profile'); ?> </label>
                        <input class="text-input" name="pass1" type="password" id="pass1" />
                    </p><!-- .form-password -->
                    <p class="form-password">
                        <label for="pass2"><?php _e('Repeat Password *', 'profile'); ?></label>
                        <input class="text-input" name="pass2" type="password" id="pass2" />
                    </p><!-- .form-password -->

                    
                    
                    
                   <p class="form-email"> <tr>
        <th scope="row"><label for="blogname"><?php _e( 'Site Title' ); ?></label></th>
        <td><input name="blogname" type="text" id="blogname" placeholder="Your Site Title" value="<?php echo $idb_blogname_1; ?>" class="regular-text" /></td>
                       </tr></p>
        
                       <p class="form-email">
        <tr>
        <th scope="row"><label for="blogdescription"><?php _e( 'Tagline' ); ?></label></th>
        <td><input name="blogdescription" type="text" id="blogdescription" placeholder="Your Tagline" aria-describedby="tagline-description" value="<?php echo $idb_blog_description; ?>" class="regular-text" />
        </td>
        </tr></p>

                    
        
      
                 
                    
                    

                    <?php 
                        //action hook for plugin and extra fields
                        do_action('show_user_profile', $profileuser); 
                    ?>
                    <p class="form-submit">
                       
                        <input name="updateuser" type="submit" id="updateuser" class="submit button" value="<?php _e('Update', 'profile'); ?>" />
                       
                        <input name="action" type="hidden" id="action" value="update-user" />
                    </p><!-- .form-submit -->
                </form><!-- #adduser -->
    
           </div>

<?php


    }

         function register_site_options_sub_page(){
        if( function_exists('acf_add_options_sub_page') ) {
    
    // add sub page
        global $theme_options_submenu;
	$theme_options_submenu = acf_add_options_sub_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title' 	=> 'Site Options',
        'parent_slug' 	=> 'rei-profile',
	));
        } //endif
    }
   
   
    
}//endclass