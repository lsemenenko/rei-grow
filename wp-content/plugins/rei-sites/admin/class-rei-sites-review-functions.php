<?php

/**
 * The settings of the plugin.
 *
 * @link       https://idbmedia.com
 * @since      1.0.0
 *
 * @package    Rei_Sites_Plugin
 * @subpackage Rei_Sites_Plugin/admin
 */

/**
 * Class WordPress_Plugin_Template_Settings
 *
 */
class Rei_Sites_Review_Functions {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

        add_shortcode( 'reviews', array($this, 'review_shortcode' ));

	}
  
        //Calculates and returns the average rating of a post's reviews.
    public static function rating_average_func()
    {
        $reviews = get_posts(array(
            'post_type' => 'rei_reviews',
            'posts_per_page'   => -1,
        ));

        $sum = 0;
        $num = 0;
        foreach ($reviews as $review) {
            $rating = get_field('rei_star_rating', $review->ID);
            if(isset($rating))
            {
                $sum += $rating;
                $num ++;
            }
        }
        $average = 0;
        if($num>0)
        {
            $average = $sum/$num;
        }
        $res = $average;
        $resfin = round($res,1);
        return $resfin;
    }

    //Calculates and returns the average rating of a post's reviews rounded to the nearest .5
    public static function rating_average_rounded_func()
    {
        $reviews = get_posts(array(
            'post_type' => 'rei_reviews',
            'posts_per_page'   => -1,
        ));

        $sum = 0;
        $num = 0;
        foreach ($reviews as $review) {
            $rating = get_field('rei_star_rating', $review->ID);
            if(isset($rating))
            {
                $sum += $rating;
                $num ++;
            }
        }
        $average = 0;
        if($num>0)
        {
            $average = $sum/$num;
        }
        $res = $average;
        $resfin = round($res*2) / 2;
        return $resfin;
    }





    //Calculates and returns the number of reviews associated with a given post
    public static function reviews_total_func()
    {
    $review_total = get_posts(array(
        'post_type' => 'rei_reviews',
        'posts_per_page'   => -1,
    ));
    return count($review_total);
    }
    

    
    	public static function review_shortcode( $atts, $content = "" ) {
            $atts = shortcode_atts( array(
		'count' => '3',
	), $atts, 'reviews' );
        $numreviews = Rei_Sites_Review_Functions::reviews_total_func();
        $avgrating = Rei_Sites_Review_Functions::rating_average_func();
        $avgratingrounded = Rei_Sites_Review_Functions::rating_average_rounded_func();
        // Pull all the related review entries for the current post.
        $reviews = get_posts(array(
            'post_type' => 'rei_reviews',
            'posts_per_page'   => $atts['count'],
        ));

        if($numreviews != 0) { 
	$content .= '<div class="reim-reviews-average"><b>Rated an average of '. $avgrating .' out of 5 stars by '. $numreviews .' reviewers.</b></div>
    <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
		<meta itemprop="ratingValue" content="'. $avgrating .'" />
    	<meta itemprop="reviewCount" content="'. $numreviews .'" />
	</span><div class="reim-review-box">'; } 

// If there are reviews to show, get the information for each review and loop through the display

if( $reviews ): foreach( $reviews as $review ):
            
		$reviewrating = get_field('rei_star_rating', $review->ID);
		$reviewtitle = get_the_title($review->ID);
        $reviewsdate = get_field('rei_reviewer_info_group_rei_review_dates', $review->ID);
		$reviewcontent = get_field('rei_review_content', $review->ID);
        $reviewer_info = get_field('rei_reviewer_info_group', $review->ID);    
		$reviewernickname = $reviewer_info['rei_nickname'];
            
        
		$content .= '<div class="reim-review-item">
			<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
				<meta itemprop="worstRating" content="1" />
				<meta itemprop="bestRating" content="5" />
				<meta itemprop="ratingValue" content="'. $reviewrating .'" />
		    </div>
		    	<b>"<span itemprop="name">'. $reviewtitle .'</span>"</b> <span class="reim-review-author" itemprop="author"> by '. $reviewernickname .'</span><br />
				<div class="star-rating" style="display: inline-block; margin-right:.5rem;">';
            
                for ($i = 0; $i < $reviewrating; $i++) { 
                    $content .= '<i class="fas fa-star" style="color: gold;"></i>';
                }
            
            $content .=  '</div><div>'. $reviewsdate .'</div>
				<div class="reim-reviews-description" itemprop="description">'. $reviewcontent .'</div>
		</div>';
                
	endforeach; 
            
	wp_reset_postdata();
                    else : 
	$content .= '<p>There are no reviews to display.</p>';
    endif;
        
 $content .= '</div>';
            
		return $content;
               
            
}
}