<?php
class Rei_Sites_SubsiteOptionPages {

 	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
    

	}
    
    
     function register_site_options_sub_page(){
        if( function_exists('acf_add_options_sub_page') ) {
    
    // add sub page
        global $theme_options_submenu;
	$theme_options_submenu = acf_add_options_sub_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title' 	=> 'Site Options',
        'parent_slug' 	=> 'rei-profile',
	));
        } //endif
    }

 
} //end class