<?php
/*
 * 
 */
if ( ! defined( 'ABSPATH' ) ) exit;
class idb_rei_oAuth_Demo { 
  private $dir;
  private $file;
  private $token;
  public function __construct( $file ) {
    $this->dir = dirname( $file );
    $this->file = $file;
    $this->token = 'idb_rei_oauth_gsheets';
    // Register plugin settings
    add_action( 'admin_init' , array( $this , 'register_settings' ) );
    // Add settings page to menu
    add_action( 'admin_menu' , array( $this , 'add_menu_item' ) );
    // Add settings link to plugins page
    //add_filter( 'plugin_action_links_' . plugin_basename( $this->file ) , array( $this , 'add_settings_link' ) );

    // NEW: setup the wp ajax action for oAuth code exchange
    add_action( 'wp_ajax_idb_rei_finish_code_exchange', array($this, 'finish_code_exchange') );
    // NEW: setup the wp ajax action to logout from oAuth
    add_action( 'wp_ajax_idb_rei_logout_from_google', array($this, 'logout_from_google') );
      
    
  }



  
  public function add_menu_item() {
      
        $page_title= 'GSheets';
        $menu_title= 'GSheets';
        $capability= 'manage_options';
        $menu_slug= 'idb_rei_oauth_settings';
        $function=  array( $this , 'settings_page' );
        $icon_url = 'data:image/svg+xml;base64,' . base64_encode('<svg xmlns="http://www.w3.org/2000/svg" width="70.46mm" height="75.06mm" viewBox="0 0 199.74 212.77"><title>house_icon_1</title><g id="graphics"><path d="M475,168.16a8.63,8.63,0,0,0-10.86-1.91L336,239.72v10.86l56.65-32.87,60.71,74a10,10,0,0,1,2.27,6.36v30.26l80.1-46.24V246.89a19.61,19.61,0,0,0-4.63-12.64Z" transform="translate(-335.95 -165.11)" style="fill:#595757"/><path d="M437.88,357.46V309.18a19.9,19.9,0,0,0-4.59-12.69l-43.06-51.77a8.48,8.48,0,0,0-10.78-1.91l-37.31,21.72A12.51,12.51,0,0,0,336,275.31V315a12.45,12.45,0,0,0,6.21,10.76l87.44,50.48a12.43,12.43,0,0,0,12.43,0l87.44-50.48A12.48,12.48,0,0,0,535.68,315V301ZM397,327.11a4.14,4.14,0,0,1-6.22,3.58l-16.42-9.48a12.45,12.45,0,0,1-6.21-10.76V289.39a4.14,4.14,0,0,1,6.21-3.59l16.42,9.49A12.44,12.44,0,0,1,397,306Z" transform="translate(-335.95 -165.11)" style="fill:#f9af2b"/></g></svg>');
        $position = null;
        
    // add options page
        global $idb_reisites_gsheets_menu;
        $idb_reisites_gsheets_menu =
        add_menu_page(
            $page_title,
            $menu_title,
            $capability,
            $menu_slug,
            $function,
            $icon_url,
            $position
        );
        }

    
    
  public function add_settings_link( $links ) {
    $settings_link = '<a href="options-general.php?page=idb_rei_oauth_settings">Settings</a>';
    array_push( $links, $settings_link );
    return $links;
  }
    
    
  public function register_settings() {
    register_setting( 'idb_rei_oauth_gsheets_group', 'idb_rei_oauth_settings' );
    add_settings_section('settingssection1', 'Google App Settings', array( $this, 'settings_section_callback'), 'idb_rei_oauth_settings');
    // you can define EVERYTHING to create, display, and process each settings field as one line per setting below.  And all settings defined in this function are stored as a single serialized object.
    add_settings_field( 'google_app_client_id', 'Google App Client ID', array( $this, 'settings_field'), 'idb_rei_oauth_settings', 'settingssection1', array('setting' => 'idb_rei_oauth_settings', 'field' => 'google_app_client_id', 'label' => '', 'class' => 'regular-text') );
    add_settings_field( 'google_app_client_secret', 'Google App Client Secret', array( $this, 'settings_field'), 'idb_rei_oauth_settings', 'settingssection1', array('setting' => 'idb_rei_oauth_settings', 'field' => 'google_app_client_secret', 'label' => '', 'class' => 'regular-text') );
    
  }
    
    
  public function settings_section_callback() { echo ' '; }
    
    
  public function settings_field( $args ) {
    // This is the default processor that will handle standard text input fields.  Because it accepts a class, it can be styled or even have jQuery things (like a calendar picker) integrated in it.  Pass in a 'default' argument only if you want a non-empty default value.
    $settingname = esc_attr( $args['setting'] );
    $setting = get_option($settingname);
    $field = esc_attr( $args['field'] );
    $label = esc_attr( $args['label'] );
    $class = esc_attr( $args['class'] );
    $default = '';
    $value = (($setting[$field] && strlen(trim($setting[$field]))) ? $setting[$field] : $default);
    echo '<input type="text" name="' . $settingname . '[' . $field . ']" id="' . $settingname . '[' . $field . ']" class="' . $class . '" value="' . $value . '" /><p class="description">' . $label . '</p>';
  }
    
    
  public function settings_page() {
    if (!current_user_can('manage_options')) {
      wp_die( __('You do not have sufficient permissions to access this page.') );
    }
    ?>
    <div class="wrap">
      <h2>GSheet Settings</h2>
      <p>You'll need to go to the <a href="https://console.developers.google.com">Google Developer Console</a> to setup your project and setup the values below.</p>
      <form action="options.php" method="POST" style="background: white; padding: 20px; border-radius: 5px; margin: 35px auto;">
        <?php settings_fields( 'idb_rei_oauth_gsheets_group' ); ?>
        <?php do_settings_sections( 'idb_rei_oauth_settings' ); ?>
        <?php submit_button(); ?>
      </form>
      
        
    <!-- We handle the login process on the settings page now -->
    <?php $this->write_out_oAuth_JavaScript(); ?>
        <div class="" id="" style="background: white; padding: 20px; border-radius: 5px; margin: 35px auto;">
        <select id='myGoogleSheets'>
        </select>
        </div>
    </div>
    <?php
  }
    
    
    
  // This function is the clearest way to get the oAuth JavaScript onto a page as needed.
  private function write_out_oAuth_JavaScript() {
    $settings = get_option('idb_rei_oauth_settings', true);
      
      if (!get_option("google_app_redirect_uri") || get_option("google_app_redirect_uri") != network_admin_url('/admin.php?page=idb_rei_oauth_redirect') ){
      
    update_option( "google_app_redirect_uri", network_admin_url('/admin.php?page=idb_rei_oauth_redirect') );
      }

    $dynamic_redirect = admin_url('/admin.php?page=idb_rei_oauth_settings');  
      
    ?>
  <script language=javascript>
      
      
  // we declare this variable at the top level scope to make it easier to pass around
  var google_access_token = "<?php echo $this->get_google_access_token(); ?>";
      
      
      
  jQuery(document).ready(function($) {
      
    var GOOGLECLIENTID = "<?php echo $settings['google_app_client_id']; ?>";
    var GOOGLECLIENTREDIRECT = "<?php echo get_option( "google_app_redirect_uri"); ?>";
    // we don't need the client secret for this, and should not expose it to the web.
      
  function requestGoogleoAuthCode() {
      
    var OAUTHURL = 'https://accounts.google.com/o/oauth2/auth';
    var SCOPE = 'profile email openid https://spreadsheets.google.com/feeds';
    var popupurl = OAUTHURL + '?scope=' + SCOPE + '&client_id=' + GOOGLECLIENTID + '&redirect_uri=' + GOOGLECLIENTREDIRECT +  '&state=<?php echo $dynamic_redirect; ?>&response_type=code&access_type=offline&prompt=select_account consent';
    var win =   window.open(popupurl, "googleauthwindow", 'width=800, height=600'); 
    var pollTimer = window.setInterval(function() { 
      try {
        if (win.document.URL.indexOf('<?php echo $dynamic_redirect; ?>') != -1) {
          window.clearInterval(pollTimer);
          var response_url = win.document.URL;
            urlpar = new URLSearchParams(response_url);
            var auth_code = urlpar.get( 'code');
           
          console.log(response_url);
          win.close();
          // We don't have an access token yet, have to go to the server for it
          var data = {
            action: 'idb_rei_finish_code_exchange',
            auth_code: auth_code
          };
          $.post(ajaxurl, data, function(response) {
            console.log(response);
            google_access_token = response;
              
            getGoogleUserInfo(google_access_token);
              
          });
        }
      } catch(e) {}    
    }, 500);
  }

      
      
  function getGoogleUserInfo(google_access_token) {
      
    $.ajax({
      url: 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + google_access_token,
      success: function(resp) {
        var user = resp;
        console.log(user);
        $('#googleUserName').text('Your Google Sheets is Authorized ' + user.name);
        $('#rei-google-image').attr('src',user.picture);
        loggedInToGoogle = true;
        $('#google-login-block').hide();
        $('#google-logout-block').show();
      },
        error: function(resp) {
        console.log('error');
      },
      dataType: "jsonp"
    });
      
    
      $.ajax({ 
    url: 'https://spreadsheets.google.com/feeds/spreadsheets/private/full?access_token=' + google_access_token + '&alt=json',
        method: 'GET',

      }).done(function(response) {
        
          console.log(response);
          
$.each(response.feed.entry, function(index, element) {
   
    $('#myGoogleSheets').append(
        $('<option></option>').val(index).html(element.title.$t)
    );
    
});
     
      }); 
    
  }
      
      
      
  function logoutFromGoogle() {
    $.ajax({
      url: ajaxurl,
      data: {
        action: 'idb_rei_logout_from_google'
      },
      success: function(resp) {
        console.log(resp);
        $('#googleUserName').text(resp);
        $('#google-login-block').show();
        $('#google-logout-block').hide();
        google_access_token = '';
      }
    });
  }
      
      
  // We also want to setup the initial click event and page status on document.ready
   $(function() {
    $('#google-login-block').click(requestGoogleoAuthCode);
    $('#google-logout-block').hide();
    $('#google-logout-block').click(logoutFromGoogle);
    // now lets show that they're logged in if they are
    if (google_access_token) {
      getGoogleUserInfo(google_access_token);
    }
   });  
  });
  </script>
<div class="" id="" style="background: white; padding: 20px; border-radius: 5px; margin: 35px auto; height: 100px;">
    <div class="" style="width:30%; float:left;">
    <img src="" alt="Profile Image" id="rei-google-image" height="100" width="100">
</div>
<div class="" style="width:70%; float: left;">
  
  <h3 id="googleUserName" style="margin: 10px 0;">You are not authorized </h3>
    <a class="btn primary-btn" style="display:inline-block; padding:0 30px; background: gainsboro; border-radius: 3px; color: darkblue; cursor: pointer;">
  <h3 id="google-logout-block">Cancel Authorization</h3></a>
    <a id="google-login-block" class="btn primary-btn" style="cursor: pointer; display:inline-block; padding:0 30px; background: gainsboro; border-radius: 3px; color: darkblue;">
    <h3>Authorize Google Sheets</h3></a>
    </div>
    </div>
  <iframe id="googleAuthIFrame" style="visibility:hidden;" width=1 height=1></iframe>
  <?php
  // END inlined JavaScript and HTML
  }
    
    
    
      public function get_google_access_token() {
      
    $expiration_time = get_option('idb_rei_google_access_token_expires', true);
    if (! $expiration_time) {
      return false;
    }
    // Give the access token a 5 minute buffer (300 seconds)
    $expiration_time = $expiration_time - 300;
    if (time() < $expiration_time) {
      return get_option('idb_rei_google_access_token', true);
    }
    // at this point we have an expiration time but it is in the past or will be very soon
      
    return $this->set_google_oauth2_token(null, 'refresh_token');
    }
    
    
    
  /* NEW section to handle doing oAuth server-to-server */
  // wrapper for wp_ajax to point to reusable function
  public function finish_code_exchange() {
      
$auth_code = ( isset( $_POST['auth_code'] ) ) ? $_POST['auth_code'] : '';
      
    echo $this->set_google_oauth2_token($auth_code, 'auth_code');
    wp_die(); 
      
  }
    
    
    
  private function set_google_oauth2_token($grantCode, $grantType) {
   /* based on code written by Jennifer L Kang that I found here
   * http://www.jensbits.com/2012/01/09/google-api-offline-access-using-oauth-2-0-refresh-token/
   * and modified to integrate with WordPress and to calculate and store the expiration date.
   */  
    $settings = get_option('idb_rei_oauth_settings', true);
    $success = true;	
    $oauth2token_url = "https://accounts.google.com/o/oauth2/token";
    $clienttoken_post = array(
      "client_id" => $settings['google_app_client_id'],
      "client_secret" => $settings['google_app_client_secret']
    );
    if ($grantType === "auth_code"){
      $clienttoken_post["code"] = $grantCode;	
      $clienttoken_post["redirect_uri"] = get_option( 'google_app_redirect_uri');
      $clienttoken_post["grant_type"] = "authorization_code";
    }
    if ($grantType === "refresh_token"){
      $clienttoken_post["code"] = get_option('idb_rei_google_refresh_token', true);
      $clienttoken_post["grant_type"] = "refresh_token";
    }
    $postargs = array(
      'body' => $clienttoken_post
     );
    $response = wp_remote_post($oauth2token_url, $postargs );
    $authObj = json_decode(wp_remote_retrieve_body( $response ), true);
    if (isset($authObj['refresh_token'])){
      $refreshToken = $authObj['refresh_token'];
      $success = update_option('idb_rei_google_refresh_token', $refreshToken, false); 
      // the final 'false' is so we don't autoload this value into memory on every page load
    }
    if ($success == get_option('idb_rei_google_refresh_token') ) {
      $success = update_option('idb_rei_google_access_token_expires',  strtotime("+" . $authObj['expires_in'] . " seconds"));
    }
    if ($grantType === "auth_code") {
      $success = update_option('idb_rei_google_access_token', $authObj[access_token], false);
      if ($success) {
        $success = $authObj[access_token];
      }
    }
    // if there were any errors $success will be false, otherwise it'll be the access token
    if (!$success) { $success=false; }
      
      
    return $success;
  }
    
    

    
    
    
  public function revoke_google_tokens() {
    /* This function finds either the access token or refresh token
     * revokes them with google (revoking the access token does the refresh too)
     * then deletes the data from the options table
    */
    $return = '';
    $token = get_option('idb_rei_google_access_token', true);
    $expiration_time = get_option('idb_rei_google_access_token_expires', true);
    if (!$token || (time() > $expiration_time)){
      $token = get_option('idb_rei_google_refresh_token', true);
    }
    if ($token) {
      $return = wp_remote_retrieve_response_code(wp_remote_get("https://accounts.google.com/o/oauth2/revoke?token=" . $token));
    } else {
      $return = "no tokens found";
    }
    if ($return == 200) {
      delete_option('idb_rei_google_access_token');
      delete_option('idb_rei_google_refresh_token');
      delete_option('idb_rei_google_access_token_expires');
      return true;
    } else {
      return $return; 
    }
  }
    
    
  // wrapper for wp_ajax to point to reusable function
  public function logout_from_google() {
    $response = $this->revoke_google_tokens();
    if ($response === true) {
      $response = "Successfully Logged Out.";
    } else {
      $response = "Didn't Log Out.";  
    }
    echo $response;
    wp_die(); 
  }
    
    /*
  public function submit_youtube_expire_request($videoid){
    $access_token = $this->get_google_access_token();
    if (! $access_token) {
      error_log("no access token for $videoid");
      return false;
    }
    $bodyargs = array(
        "id" => $videoid,
        "kind" => "youtube#video",
        "status" => array(
          "privacyStatus" => "private"
        )
      );
    $body = json_encode($bodyargs);
    $url = "https://www.googleapis.com/youtube/v3/videos?part=status&fields=status";
    $args = array(
      "method" => "PUT",
      "headers" => array(
        "Authorization" => "Bearer " . $access_token,
        "Content-Type" => "application/json"
      ),
      "body" => $body
    );
    $request = wp_remote_request($url, $args);
    if (wp_remote_retrieve_response_code($request) != 200){
      error_log("privacy set failed : " . wp_remote_retrieve_body($request));
      return false;
    }
    return json_decode(wp_remote_retrieve_body($request));
  }
    
  */
//end of class  
}
// Instantiate our class
global $plugin_obj;
$plugin_obj = new idb_rei_oAuth_Demo( __FILE__ );
// always cleanup after yourself
register_deactivation_hook(__FILE__, 'idb_rei_deactivation');
function idb_rei_deactivation() {
  // delete the google tokens
  $plugin_obj = new idb_rei_oAuth_Demo( __FILE__ );
  $plugin_obj->revoke_google_tokens();
  error_log('GSheets has been deactivated');
}
/* END OF FILE */