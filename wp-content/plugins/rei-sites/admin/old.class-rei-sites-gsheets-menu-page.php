<?php
class Rei_Sites_GSheetsMenuPage {

 	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
    

	}
    
    public function reisites_gsheets_page() {
        
        $page_title= 'GSheets';
        $menu_title= 'GSheets';
        $capability= 'manage_options';
        $menu_slug= 'rei-sites-gsheets';
        $function= array($this,'render_settings_page_content');
        $icon_url = 'data:image/svg+xml;base64,' . base64_encode('<svg xmlns="http://www.w3.org/2000/svg" width="70.46mm" height="75.06mm" viewBox="0 0 199.74 212.77"><title>house_icon_1</title><g id="graphics"><path d="M475,168.16a8.63,8.63,0,0,0-10.86-1.91L336,239.72v10.86l56.65-32.87,60.71,74a10,10,0,0,1,2.27,6.36v30.26l80.1-46.24V246.89a19.61,19.61,0,0,0-4.63-12.64Z" transform="translate(-335.95 -165.11)" style="fill:#595757"/><path d="M437.88,357.46V309.18a19.9,19.9,0,0,0-4.59-12.69l-43.06-51.77a8.48,8.48,0,0,0-10.78-1.91l-37.31,21.72A12.51,12.51,0,0,0,336,275.31V315a12.45,12.45,0,0,0,6.21,10.76l87.44,50.48a12.43,12.43,0,0,0,12.43,0l87.44-50.48A12.48,12.48,0,0,0,535.68,315V301ZM397,327.11a4.14,4.14,0,0,1-6.22,3.58l-16.42-9.48a12.45,12.45,0,0,1-6.21-10.76V289.39a4.14,4.14,0,0,1,6.21-3.59l16.42,9.49A12.44,12.44,0,0,1,397,306Z" transform="translate(-335.95 -165.11)" style="fill:#f9af2b"/></g></svg>');
        $position = null;
        
    // add options page
        global $idb_reisites_gsheets_menu;
        $idb_reisites_gsheets_menu =
    add_menu_page(
        $page_title,
        $menu_title,
        $capability,
        $menu_slug,
        $function,
        $icon_url,
        $position
    );
        }
    
    


	/**
	 * Initializes idb-sbp's display options page by registering the Sections,
	 * Fields, and Settings.
	 *
	 * This function is registered with the 'admin_init' hook.
	 */
	public function initialize_rei_sites_gsheets_options() {
     
		// register the fields
		register_setting( 'rei_sites_gsheets_options', 'rei_sites_app_url' );

	} // end 



	/**
	 * Renders page display for the menu defined above.
	 */
	public function render_settings_page_content(  ) { ?>

		<!-- Create a header in the default WordPress 'wrap' container -->
		<div class="wrap">

			<h2><?php _e( 'Google Sheets API', 'rei-sites' ); ?></h2>
			<?php settings_errors(); ?>

			
            <div class="rei-sites-option-container" style="background: white; padding: 40px; border-radius: 5px; margin: 30px 0;">
                <h2><?php _e( 'REI Sites App Settings', 'rei-sites' ); ?></h2>
			<form method="post" action="options.php">

                      <?php settings_fields( 'rei_sites_gsheets_options' );
                do_settings_sections( 'rei_sites_gsheets_options' );?>

			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" valign="top">Google Sheets App URL</th>
						<td>

							<input id="rei_sites_app_url" name="rei_sites_app_url" type="url" class="regular-text" value="<?php echo get_option('rei_sites_app_url'); ?>" />
							
						</td>
					</tr>
						
                      
                        </tbody>
			             </table>
        



				

<?php submit_button(); ?>
			</form>
                
               

            </div>
		</div><!-- /.wrap -->
	<?php
	}





    
} //end class