<?php

/**
 * The settings of the plugin.
 *
 * @link       https://idbmedia.com
 * @since      1.0.0
 *
 * @package    Rei_Sites_Plugin
 * @subpackage Rei_Sites_Plugin/admin
 */

define( 'REI_ADMIN_DIR', plugin_dir_path( '/', __FILE__ ) . 'admin/'  );

/**
 * Class Rei_Sites_Admin_Theme
 *
 */
class Rei_Sites_Admin_Theme {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}
    
    
    function reiAdminTheme() {
        
    }
    
    function idb_rpp_enqueue_styles() {
    wp_enqueue_style( 'rei_admin', REI_ADMIN_DIR . '/css/rei-sites-admin.css');}
    
}