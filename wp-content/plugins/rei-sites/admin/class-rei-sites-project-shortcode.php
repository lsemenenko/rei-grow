<?php

/**
 * The settings of the plugin.
 *
 * @link       https://idbmedia.com
 * @since      1.0.0
 *
 * @package    Rei_Sites_Plugin
 * @subpackage Rei_Sites_Plugin/admin
 */

/**
 * Class WordPress_Plugin_Template_Settings
 *
 */
class Rei_Sites_Project_Shortcode {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

        add_shortcode( 'projects', array($this, 'project_shortcode' ));

	}

    //Calculates and returns the number of projects associated with a given post
    public static function projects_total_func()
    {
		$project_total = get_posts(array(
			'post_type' => 'rei_projects',
			'posts_per_page'   => -1,
		));
    	return count($project_total);
    }

    
    public static function project_shortcode( $atts, $content = "" ) {
        $atts = shortcode_atts( array('count' => '3',), $atts, 'projects' );
    $numprojects = self::projects_total_func();
        
        // Pull all the related project entries for the current post.
        $projects = get_posts(array(
            'post_type' => 'rei_projects',
            'posts_per_page'   => $atts['count'],
        ));


        
if($numprojects != 0) { 
    $content .='<span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
    	<meta itemprop="reviewCount" content="'. $numprojects .'" />
	</span><div class="rei-project-box">'; }
            
        
// If there are projects to show, get the information for each review and loop through the display
            
        if( $projects ): foreach( $projects as $project ):
        
        $projectimage = get_field('rei_project_image', $project->ID);
        $projecttitle = get_field('rei_project_title', $project->ID);
        $projectsubtitle = get_field('rei_project_subtitle', $project->ID);
        $projectcontent1 = get_field('rei_project_column_one_content', $project->ID);
        $projectcontent2 = get_field('rei_project_column_two_content', $project->ID);
        $projectcontent3 = get_field('rei_project_column_three_content', $project->ID);   
            
		$content .= '<div class="rei-project-item"><div class="rei-project-image" style="background-image:url(' .$projectimage. ')"></div><div class="rei-project-content-box"><div class="rei-project-title-container"><div class="rei-project-title">'. $projecttitle. '</div><div class="rei-project-subtitle">'. $projectsubtitle. '</div></div><div class="rei-project-column-container"><div class="rei-column-content">'. $projectcontent1. '</div><div class="rei-column-content">'. $projectcontent2. '</div><div class="rei-column-content-last">'. $projectcontent3. '</div></div></div></div>';
        
        endforeach; 
            
	wp_reset_postdata();
        else : 
		$content .= '<p>There are no projects to display.</p>';
    	endif;
 		$content .= '</div>'; 
		return $content;
                    
	}
}