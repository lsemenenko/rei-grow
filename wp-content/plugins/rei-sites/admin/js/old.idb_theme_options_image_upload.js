jQuery(function($){

	$('body').on('click', '.misha_upload_image_button', function(e){
		e.preventDefault();
 
    		var button = $(this),
    		    custom_uploader = wp.media({
			title: 'Insert image',
			library : {
				type : 'image'
			},
			button: {
				text: 'Use this image' // button label text
			},
			multiple: false 
		}).on('select', function() { // it also has "open" and "close" events 
			var attachment = custom_uploader.state().get('selection').first().toJSON();
                    $('.image-preview-message').remove()
                    $('.idb_image_logo_null').attr('class', 'newClass');
            $('#idb_cto_ss_1').val(attachment.url);
                    $(".image-preview-1").attr("src",attachment.url);

		})
		.open();
	});
 
	/*
	 * Remove image event
	 */
	$('body').on('click', '.misha_remove_image_button', function(){
		$('.image-preview-1').removeAttr('src');
       $( ".image-preview" ).attr('class', 'idb_image_logo_null');
        
         $('#idb_cto_ss_1').val('');
        $( ".idb_image_logo_null" ).add( "<h5 class='image-preview-message'>Save Your Settings</h5>" ).appendTo( ".idb_image_logo_null" );
		return false;
	});
 
});