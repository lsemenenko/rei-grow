<?php
class reiAddNetworkPages {

	function __construct() {
        add_action( 'network_admin_menu', array( $this, 'reisites_admin_menu' ) );
		add_action( 'admin_init', array( $this, 'register_reisites_settings'  ) );
        add_action('network_admin_edit_idb_reisites_update_network_options_1',array( $this, 'idb_reisites_update_network_options_1'));
        add_action( 'network_admin_menu' , array( $this , 'add_hidden_redirect_page' ) );
	}

	public function reisites_admin_menu() {
        $page_title= 'Rei Sites Settings';
        $menu_title= 'Rei Sites';
        $capability= 'manage_network_options';
        $menu_slug= 'idb-reisites-settings';
        $function= array($this,'reisites_top_menu_callback');
        $icon_url = 'data:image/svg+xml;base64,' . base64_encode('<svg xmlns="http://www.w3.org/2000/svg" width="70.46mm" height="75.06mm" viewBox="0 0 199.74 212.77"><title>house_icon_1</title><g id="graphics"><path d="M475,168.16a8.63,8.63,0,0,0-10.86-1.91L336,239.72v10.86l56.65-32.87,60.71,74a10,10,0,0,1,2.27,6.36v30.26l80.1-46.24V246.89a19.61,19.61,0,0,0-4.63-12.64Z" transform="translate(-335.95 -165.11)" style="fill:#595757"/><path d="M437.88,357.46V309.18a19.9,19.9,0,0,0-4.59-12.69l-43.06-51.77a8.48,8.48,0,0,0-10.78-1.91l-37.31,21.72A12.51,12.51,0,0,0,336,275.31V315a12.45,12.45,0,0,0,6.21,10.76l87.44,50.48a12.43,12.43,0,0,0,12.43,0l87.44-50.48A12.48,12.48,0,0,0,535.68,315V301ZM397,327.11a4.14,4.14,0,0,1-6.22,3.58l-16.42-9.48a12.45,12.45,0,0,1-6.21-10.76V289.39a4.14,4.14,0,0,1,6.21-3.59l16.42,9.49A12.44,12.44,0,0,1,397,306Z" transform="translate(-335.95 -165.11)" style="fill:#f9af2b"/></g></svg>');
        $position = null;
        
    // add options page
        global $idb_reisites_topmenu;
        $idb_reisites_topmenu =
    add_menu_page(
        $page_title,
        $menu_title,
        $capability,
        $menu_slug,
        $function,
        $icon_url,
        $position
    );
        }
    
    
public function add_hidden_redirect_page() {
    add_submenu_page( '__doesnt_exist', 'OAuth Redirect Page' , 'OAuth Redirect Page' , 'manage_network_options' , 'idb_rei_oauth_redirect' ,  array( $this , 'oauth_redirect_callback' ) );
  }
    
    public function oauth_redirect_callback(){
        

    if (isset($_GET['state']) ){ 
        
$state_redirect = $_GET['state'];
        $code = $_GET['code'];

        echo $state_redirect.'&code='.$code;
        wp_redirect($state_redirect.'&code='.$code);
        
        
      
    }
        echo '<div style="padding:40px; margin: 50px auto;"><h2>This page is for GSheets integration, do NOT delete.</h2></div>';

    }



function register_reisites_settings() {
	//register our settings
	register_setting( 'idb-reisites-settings', 'reisites_show_services');

}
 
function reisites_top_menu_callback(){ ?>
    <style>
        #wpcontent {
    height: 100%;
    padding-left: 0px;
    background: #f1f1f1;
    background: -webkit-linear-gradient( #606c88, #3f4c6b);
    background-repeat: no-repeat;
    background-size: cover;
    background-attachment: fixed;
}
       .reisites-network-email-list {
    background: #32373c;
    padding: 5px 20px;
}
</style>
<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="//www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins">
<body>

<!-- !PAGE CONTENT! -->
<div class="w3-main idb-container" style="margin-left:40px;margin-right:40px">

  <!-- Header -->
    <!-- Home -->
  <div class="w3-container" id="" style="margin-top:75px">
    <h1 class="w3-jumbo w3-text-white"><b>Rei Sites</b></h1>
    <h1 class="w3-xxxlarge w3-text-red"><b>Take Control.</b></h1>
    <hr style="width:50px;border:5px solid red" class="w3-round">
    <p class="w3-text-white"><strong>Network Options</strong></p>

<div class=""> 

    <h3 class="w3-text-white">Secure Multisite Email Setup:</h3>
 <p class="w3-text-white">   
<strong>Copy this into your wp-config file. Replace the values with your own.</strong><br/><br/>
     </p>
<div class="reisites-network-email-list"> 
    <p class="w3-text-white"> 
Text Here.
     </p>
    </div>
    
   

<form method="post" action="edit.php?action=idb_reisites_update_network_options_1">
    <?php settings_fields( 'idb-reisites-settings' ); ?>
    <?php do_settings_sections( 'idb-reisites-settings' ); 
    
  if (isset($_GET['updated'])): ?>
<div id="message" class="updated notice is-dismissible"><p><?php _e('DentaSite Options Saved!') ?></p></div>
  <?php endif; ?>

        <table class="form-table">
        
        
        <h3 class="idb_nty_options_title">White Label Options</h3>
       

        
        <tr valign="top">
        <th scope="row"><h5>Admin Menu Title</h5></th>
        <td><input type="text" name="idb_menu_title_option" id="idb_menu_title_option" value="<?php echo esc_attr( get_site_option('idb_menu_title_option') ); ?>" /></td>
        </tr>
   
        
        
        <tr valign="top">
        <th scope="row"><h5>Access</h5>
            <p>Text Here.<br/>
                More text here.</p>
            </th>
        <td><input type="checkbox" class="tgl tgl-ios" id="cb1" name="reisites_show_services" <?php if ( get_site_option('reisites_show_services')) {echo 'checked';} ?>/>
             <label class="tgl-btn" for="cb1"></label>
            </td>
        </tr>
         
        
        

        
    </table>
    
    
    <?php submit_button(); ?>

</form>
</div>
      
          </div>
    </div>


</body>

<?php } 
    
 
        
        
    /**
 * This function here is hooked up to a special action and necessary to process
 * the saving of the options. This is the big difference with a normal options
 * page.
 */

function idb_reisites_update_network_options_1() {
  // Make sure we are posting from our options page. There's a little surprise
  // here, on the options page we used the 'post3872_network_options_page'
  // slug when calling 'settings_fields' but we must add the '-options' postfix
  // when we check the referer.
  check_admin_referer('idb-reisites-settings-options');
 
  // This is the list of registered options.
  global $new_whitelist_options;
  $options = $new_whitelist_options['idb-reisites-settings'];
 
  // Go through the posted data and save only our options. This is a generic
  // way to do this, but you may want to address the saving of each option
  // individually.
  foreach ($options as $option) {
    if (isset($_POST[$option])) {
      // Save our option with the site's options.
      // If we registered a callback function to sanitizes the option's
      // value it will be called here (see register_setting).
      update_site_option($option, $_POST[$option]);
    } else {
      // If the option is not here then delete it. It depends on how you
      // want to manage your defaults however.
      delete_site_option($option);
    }
  }
 
  // At last we redirect back to our options page.
  wp_redirect(add_query_arg(array('page' => 'idb-reisites-settings',
      'updated' => 'true'), network_admin_url('admin.php')));
  exit;
}
 
    
}//endclass