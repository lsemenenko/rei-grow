<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://idbmedia.com
 * @since      1.0.0
 *
 * @package    Rei_Sites
 * @subpackage Rei_Sites/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Rei_Sites
 * @subpackage Rei_Sites/admin
 * @author     J Hanlon - IDB Media <j@roselane.com>
 */
class Rei_Sites_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
        
        $this->load_dependencies();

	}
    
    /**
	 * Load the required dependencies for the Admin facing functionality.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Rei_Sites_Plugin_Admin_Settings. Registers the admin settings and page.
	 *
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {


        
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles($hook) {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . '/css/rei-sites-admin.css', array(), $this->version, 'all' );
        if( $hook == 'toplevel_page_rei-profile' ){
        wp_enqueue_style( 'profile_style', plugin_dir_url( __FILE__ ) . 'css/idb_rei_profile_style.css' , array(), $this->version, 'all' );
        }
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts($hook) {

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/rei-sites-admin.js', array( 'jquery' ), $this->version, false );
        
        // if( $hook == 'toplevel_page_rei-profile' ){
        // wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . '/js/idb_theme_options_image_upload.js', array( 'jquery' ), $this->version, true );
        // }
        
	}

}
