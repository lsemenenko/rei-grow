<?php
class Idb_Rpp_loadAjax {

    public function __construct() {
            add_action( 'wp_enqueue_scripts', array($this, 'idb_rpp_enqueue_scripts' ));
         // add_action( 'wp_print_styles', array($this, 'idb_rpp_enqueue_styles' ), 999);

            add_filter( 'the_content', array( $this, 'report_button' ), 10, 1 );
            add_action( 'wp_ajax_nopriv_idb_rpp_resume', array( $this, 'idb_rpp_resume_post' ) );
            add_action( 'wp_ajax_idb_rpp_resume', array( $this, 'idb_rpp_resume_post' ) );
            add_action( 'phpmailer_init', array( $this, 'send_smtp_email' ));
        }


    
    
    function send_smtp_email( $phpmailer ) {
        $phpmailer->isSMTP();
        $phpmailer->Host       = 'smtp.mailgun.org';
        $phpmailer->SMTPAuth   = true;
        $phpmailer->Port       = '2525';
        $phpmailer->Username   = 'postmaster@resumepagepro.com';
        $phpmailer->Password   = 'qG2hsK9r+BYM';
        $phpmailer->SMTPSecure = 'tls';
        $phpmailer->From       = 'info@resumepagepro.com';
        $phpmailer->FromName   = 'Kitchenbeats';
    }
    
    
    
    
    
    
    public function report_button( $content ) {

        
        
    // display button only on posts
    if ( ! is_page('contact') ) {
        return $content;
    }

        
        $nonce = wp_create_nonce( 'idb_rpp_email_nonce_' . get_the_ID() );
        
    $content .= '
    <div class="report-a-bug">

<form id="idb_rpp_contact_form">
<fieldset>
<label for="fname" generated="true">First Name</label>
<label for="fname" generated="true" class="error"></label>
<input type="text" class="idb-rpp-form-content" id="fname" name="firstname" placeholder="Your name.." data-rule-minlength="4" data-rule-maxlength="30" data-msg-minlength="At least four chars" data-msg-maxlength="At most thirty chars" required data-msg="Please fill this field"/>

<label>Last Name</label>
<label for="lname" generated="true" class="error"></label>
<input type="text" class="idb-rpp-form-content" id="lname" name="lastname" placeholder="Your last name.." data-rule-minlength="4" data-rule-maxlength="30" data-msg-minlength="At least four chars" data-msg-maxlength="At most thirty chars" required data-msg="Please fill this field"/>


<label>Email</label>
<label for="rpp_contact_email" generated="true" class="error"></label>
<input type="email" class="idb-rpp-form-content" id="rpp_contact_email" name="email" placeholder="Your email" required/>

<label>Subject</label>
<label for="subject" generated="true" class="error"></label>
<textarea class="idb-rpp-form-content" id="subject" name="rpp_contact_message" placeholder="Write something.." required></textarea>
<input class="send-report" type="submit" value="Submit" data-nonce="'. $nonce .'"  data-post_id="' . get_the_ID() . '"/>
  </fieldset>
</form>
                </div>';

    return $content;

}
    
   function idb_rpp_resume_post() {

    $data = $_POST;
       
       if ( check_ajax_referer( 'idb_rpp_email_nonce_' . $data['post_id'], 'nonce', false ) == false ) {
        wp_send_json_error('Something messed up :/');
    }
       
    $post_title = get_the_title( intval( $data['post_id'] ) );
       
        $to = 'kitchenbeats@gmail.com';
        $subject = 'The subject';
        $body = $data['form_contents'][3]['value'];
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail( $to, $subject, $body);


    wp_send_json_success( __( 'Email Sent!', 'idb_rpp' ) );

}

  
                
        function idb_rpp_enqueue_scripts() {
            
            if (is_page('contact') || is_page('fl-theme-layout/contact-1/') || is_page('fl-theme-layout/contact/')){

            wp_enqueue_style( 'idb_rpp_resume', REI_PLUGIN_URL .'public/css/idb_rpp_contact_style.css' );

            // wp_enqueue_script( 'idb_frontend_post', IDB_DENTASITE_URL .'assets/js/idb_contenteditable.js', array('jquery'), '', 'true');

                
            wp_enqueue_script( 'idb_rpp_resume', REI_PLUGIN_URL .'public/js/ajax_email_scripts.js', array('jquery'), null, true );
             

            // set variables for script
            wp_localize_script( 'idb_rpp_resume', 'settings', array(
            'ajaxurl'    => admin_url( 'admin-ajax.php' ),
            'send_label' => __( 'Send report', 'reportabug' )
            ) );

            } //end if


            }
    
    
    }//endclass


new Idb_Rpp_loadAjax();