<?php

/**
 * The settings of the plugin.
 *
 * @link       https://idbmedia.com
 * @since      1.0.0
 *
 * @package    Rei_Sites_Plugin
 * @subpackage Rei_Sites_Plugin/admin
 */

/**
 * Class Rei_Sites_Admin_Theme
 *
 */
class Rei_Sites_FrontendForms {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}


    
    function add_acf_head() {
        acf_form_head();
    }
    function idb_rei_deregister_admin_styles() {
	wp_deregister_style( 'wp-admin' );
    }
    function idb_rei_acf_form_edit_toggle() { ?>
 <?php if ( current_user_can('manage_options') ){ ?>
    	 <button id="edit-toggle">Theme <i class="fa fa-cog" ></i> </button>
 <?php } ?>
    <?php } 
    
    function idb_rei_acf_form_content() { ?>

	<div id="edit-post">

    	<?php
		$edit_post = array(
			'post_id'            => 'options', // Get the post ID
			'field_groups'       => array('group_5cddd81cd4d51',), // Create post field group ID(s)
			'form'               => true,
		
			'html_before_fields' => '<div class="edit-close-wrap"><button class="edit-close" role="button" aria-pressed="false"><i class="fa fa-times"></i> Close</button></div>',
			'html_after_fields'  => '',
			'submit_value'       => 'Save Changes',
		);
		acf_form( $edit_post );
		?>

	</div>
    <?php }
    
    
    
} //end class