(function($) {
    
$(document).ready( function() {
    
   
    var target; // variable for event target to get id of clicked element
	var file_frame_1; // variable for the wp.media file_frame
	
	// attach a click event to element
	$( '#rei-hero-image .rpp-button-change-image' ).on( 'click', function( event ) {
        var $this = $(this); 
        target = event.target.id;
		event.preventDefault();

        // if the file_frame has already been created, just reuse it
		if ( file_frame_1 ) {
			file_frame_1.open();
			return;
		} 

		file_frame_1 = wp.media.frames.file_frame_1 = wp.media({
			title: $this.data( 'uploader_title' ),
			button: {
				text: $this.data( 'uploader_button_text' ),
			},
			multiple: false // set this to true for multiple file selection
		});

		file_frame_1.on( 'select', function() {
			attachment = file_frame_1.state().get('selection').first().toJSON();

			$("#rei-hero-image").css("background-image", "linear-gradient(0deg,rgba(0,0,0,0.2),rgba(0,0,0,0.2)), url(" + attachment.url + ")").css("background-position", "center center").attr('data-id',attachment.id);
          
		});

		file_frame_1.open();
	});
});
    
    // next js function
        var testMyBtn = document.getElementById("submit-2"); 
    
    $( ".edit-bar-btn" ).click(function() {
  $( "#followTab" ).toggleClass( 'opened-edit-bar');
  $( "#rpp-profile-image" ).toggleClass( 'rei-hide-button');
  $( ".rei-content-edit-bar" ).toggleClass( 'rei-hide-button');
     $( ".rei-editor-select" ).hide();
        //$( ".rei-content-edit-bar" ).hide();
$('[contenteditable]').attr('contenteditable') === 'true' ? 
$('[contenteditable]').attr('contenteditable', 'false') : 
$('[contenteditable]').attr('contenteditable', 'true');

    });
        
    // $( ".rei-content-edit-bar" ).hide();
    
$(function() {
    
    
    
    $("*").click(function() {
        if ($(this).attr("contentEditable") == "true" && $(this).hasClass('rei-font-styling-on')) {
            $( ".rei-editor-select" ).show();
           
        } else if ($(this).attr("contentEditable") == "true" ) {
            $( ".rei-editor-select" ).hide("slow"); 
        }
           });
      /*  
    $("*").click(function(e) {
         if ($(this).attr("contentEditable") == "true" ) {
            $( ".rei-content-edit-bar" ).show(); 
            e.stopPropagation();
        } else {
            $( ".rei-content-edit-bar" ).hide();
             
        } 
        
        }); */
    
           $(".rei-editor-select-fontsize").change(function() {
            document.execCommand("formatBlock", false, $(this).val());
       })
    });
    

    
    
    
    if(testMyBtn){ 
    testMyBtn.addEventListener("click", function(e) {
    e.preventDefault(); 
        
       
        
        
 $.confirm({

     title: 'Update',

      content: 'Are you sure? You will not be able to undo!',

       buttons: {

        confirm: function () {  
$(testMyBtn).html('<div class="loading-container"><div class="loading"></div></div>'); 
        
        var reiPageId = $('#rei-page-id').attr("class");
        if($(".rei-content-title").text().trim()){
        var reiMainContentTitle = $(".rei-content-title").text().trim();
        }
        if(document.getElementById("rei-main-content").innerHTML){   
        var reiMainContent = document.getElementById("rei-main-content").innerHTML;
        }
        if($("#rei-hero-image").css('background-image')){    
        var reiHeroImage = $("#rei-hero-image").css('background-image').match(/url\([^\)]+\)/gi);
        reiHeroImage = reiHeroImage[0].replace('url("','').replace('")',''); 
        var reiHeroImageID = $("#rei-hero-image").attr('data-id');   
        }
        if($('.rei-hero-title').html()){
        var reiHeroTitle = $('.rei-hero-title').html();
        }
        if($('.rei-hero-subtitle').html()){
        var reiHeroSubtitle = $('.rei-hero-subtitle').html();
        }
        if($('.rei-hero-content').html()){
        var reiHeroContent = $('.rei-hero-content').html();
        }  
        if($('.rei-add-form-title').html()){
        var reiAddFormTitle = $('.rei-add-form-title').html();
        }
        if($('.rei-add-form-content').html()){
        var reiAddFormContent = $('.rei-add-form-content').html();
        }
        if($('.rei-quicklinks-title').html()){
        var reiQuicklinksTitle = $('.rei-quicklinks-title').html();
        }
        if($('.rei-quicklinks-content').text().trim()){
        var reiQuicklinksContent = [...document.querySelectorAll(".rei-quicklinks-content")].map(e=>e.innerText.trim())[0];
        }
        if($('.rei-projects-title').html()){
        var reiProjectsTitle = $('.rei-projects-title').html();
        }
        if($('.rei-projects-subtitle').html()){    
        var reiProjectsSubTitle = $('.rei-projects-subtitle').html();
        }
        if($('.rei-projects-content').text().trim()){
        var reiProjectsContent = [...document.querySelectorAll(".rei-projects-content")].map(e=>e.innerText.trim())[0];
        }
        if($('.reim-reviews-title').html()){
        var reiReviewsTitle = $('.reim-reviews-title').html();
        }
        if($('.reim-reviews-subtitle').html()){    
        var reiReviewsSubTitle = $('.reim-reviews-subtitle').html();
        }
        if($('.reim-reviews-content').text().trim()){
        var reiReviewsContent = [...document.querySelectorAll(".reim-reviews-content")].map(e=>e.innerText.trim())[0];
        }
        if($('.reim-faq-title').html()){    
        var reiFaqTitle = $('.reim-faq-title').html();
        }
        if($(".reim-faq-question").text().trim()){    
        var reiFaqQuestions = [...document.querySelectorAll(".reim-faq-question")].map(e=>e.innerText.trim());
        }
        if([...document.querySelectorAll(".reim-faq-answer")].map(e=>e.innerText.trim())){
        var reiFaqAnswers = [...document.querySelectorAll(".reim-faq-answer")].map(e=>e.innerText.trim());
        }
        if($('.rei-bus-name').html()){
        var reiBusinessName = $('.rei-bus-name').html();
        }
        
      
         

        
        //console.log(ExperienceArray);

        
        var str = {
			'action': 'rpp_resume_ajax_hook',
            'reiBusinessName' : reiBusinessName,
            'reiHeroImage': reiHeroImage,
            'reiHeroImageID': reiHeroImageID,
            'reiHeroTitle' : reiHeroTitle,
            'reiHeroSubtitle' : reiHeroSubtitle,
            'reiHeroContent' : reiHeroContent,
            'reiAddFormTitle' : reiAddFormTitle,
            'reiAddFormContent' : reiAddFormContent,
            'reiMainContentTitle' : reiMainContentTitle,
            'reiMainContent' : reiMainContent,
            'reiPageId' : reiPageId,
            'reiQuicklinksTitle' : reiQuicklinksTitle,
            'reiQuicklinksContent' : reiQuicklinksContent,
            'reiProjectsTitle' : reiProjectsTitle,
            'reiProjectsSubTitle' : reiProjectsSubTitle,
            'reiProjectsContent' : reiProjectsContent,
            'reiReviewsTitle' : reiReviewsTitle,
            'reiReviewsSubTitle' : reiReviewsSubTitle,
            'reiReviewsContent' : reiReviewsContent,
            'reiFaqTitle' : reiFaqTitle,
            'reiFaqQuestions' : reiFaqQuestions,
            'reiFaqAnswers' : reiFaqAnswers,
            'rpp_resume_ajax_security' : resume_settings.rpp_resume_ajax_security,
		};
            
        
            $.ajax({
        type: 'POST', 
        url: resume_settings.resume_ajaxurl, 
             dataType: "html",   
                 data : str,
                success: function(result) {

                $(testMyBtn).text('Save'); 
                $('[contenteditable]').attr('contenteditable') === 'true' ? 
                $('[contenteditable]').attr('contenteditable', 'false') : 
                $('[contenteditable]').attr('contenteditable', 'true');
                $( "#followTab" ).toggleClass( 'opened-edit-bar');
                $( "#rpp-profile-image" ).toggleClass( 'rei-hide-button');
                $( ".rei-content-edit-bar" ).toggleClass( 'rei-hide-button');
                    
                
                console.log('success');
                    console.log(str);
                    
                },
                error: function(result) {
                  
                console.log('Somethings messed up!');
                    
                }
         })
                             },      
         cancel: function () { 
            $('[contenteditable]').attr('contenteditable') === 'true' ? 
            $('[contenteditable]').attr('contenteditable', 'false') : 
            $('[contenteditable]').attr('contenteditable', 'true');
            $( "#followTab" ).toggleClass( 'opened-edit-bar');
            $( "#rpp-profile-image" ).toggleClass( 'rei-hide-button');
            $( ".rei-content-edit-bar" ).toggleClass( 'rei-hide-button');
           
        } 
       }// end buttons

        });
    })
        }

})(jQuery);