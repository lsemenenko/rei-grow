<?php
if (is_admin()){

    
     // idb Ajax function
     function rpp_resume_ajax_hook(){
        //Ajax Security Nonce
        if ( ! check_ajax_referer( 'idb-rpp-resume-security-nonce', 'rpp_resume_ajax_security' ) && !current_user_can('edit_others_pages') ) {
        wp_send_json_error( 'Invalid security token sent.' );
        wp_die();
        }

        if (isset($_POST[ 'reiHeroImage' ])){
         $rei_hero_image = $_POST[ 'reiHeroImage' ];
        }
         if (isset($_POST[ 'reiHeroImageID' ])){
         $rei_hero_image_ID = $_POST[ 'reiHeroImageID' ];
        }
         if (isset($_POST[ 'reiHeroTitle' ])){
         $reiHeroTitle = $_POST[ 'reiHeroTitle' ];
        }
         if (isset($_POST[ 'reiHeroSubtitle' ])){
         $rei_hero_subtitle = $_POST[ 'reiHeroSubtitle' ];
        }
         if (isset($_POST[ 'reiHeroContent' ])){
         $rei_hero_content = $_POST[ 'reiHeroContent' ];
        }
         
         if (isset($_POST[ 'reiMainContentTitle' ])){
         $reiMainContentTitle = $_POST[ 'reiMainContentTitle' ];
        }
         if (isset($_POST[ 'reiMainContent' ])){
         $reiMainContent = $_POST[ 'reiMainContent' ];
        }
         if (isset($_POST[ 'reiLeadMagnetTitle' ])){
         $reiLeadMagnetTitle = $_POST[ 'reiLeadMagnetTitle' ];
        }
         
         if (isset($_POST[ 'reiAddFormTitle' ])){
         $rei_add_form_title = $_POST[ 'reiAddFormTitle' ];
        }
          if (isset($_POST[ 'reiAddFormContent' ])){
         $rei_add_form_content = $_POST[ 'reiAddFormContent' ];
        }
         if (isset($_POST[ 'reiQuicklinksTitle' ])){
         $reiQuicklinksTitle = $_POST[ 'reiQuicklinksTitle' ];
        }
         if (isset($_POST[ 'reiQuicklinksContent' ])){
         $reiQuicklinksContent = $_POST[ 'reiQuicklinksContent' ];
        }
         if (isset($_POST[ 'reiProjectsTitle' ])){
         $reiProjectsTitle = $_POST[ 'reiProjectsTitle' ];
        }
         if (isset($_POST[ 'reiProjectsSubTitle' ])){
         $reiProjectsSubTitle = $_POST[ 'reiProjectsSubTitle' ];
        }
         if (isset($_POST[ 'reiProjectsContent' ])){
         $reiProjectsContent = $_POST[ 'reiProjectsContent' ];
        }
         if (isset($_POST[ 'reiReviewsTitle' ])){
         $reiReviewsTitle = $_POST[ 'reiReviewsTitle' ];
        }
         if (isset($_POST[ 'reiReviewsSubTitle' ])){
         $reiReviewsSubTitle = $_POST[ 'reiReviewsSubTitle' ];
        }
         if (isset($_POST[ 'reiReviewsContent' ])){
         $reiReviewsContent = $_POST[ 'reiReviewsContent' ];
        }
         if (isset($_POST[ 'reiFaqTitle' ])){
         $reiFaqTitle = $_POST[ 'reiFaqTitle' ];
        }
         if (isset($_POST[ 'reiFaqQuestions' ])){
         $reiFaqQuestions = $_POST[ 'reiFaqQuestions' ];
        }
         if (isset($_POST[ 'reiFaqAnswers' ])){
         $reiFaqAnswers = $_POST[ 'reiFaqAnswers' ];
        }
         if (isset($_POST[ 'reiPageId' ])){
         $reiPageId = $_POST[ 'reiPageId' ];
        }

         $field_objects = get_field_objects($reiPageId);
         
        //Hero Group 
        $HeroGroupKey = $field_objects['reim_hero']['key'];
        $hero_image_key = $field_objects['reim_hero']['sub_fields'][0]['key'];
        $hero_title_key = $field_objects['reim_hero']['sub_fields'][1]['key'];
        $hero_subtitle_key = $field_objects['reim_hero']['sub_fields'][2]['key'];
        $hero_content_key = $field_objects['reim_hero']['sub_fields'][3]['key'];
 
         
        // update the Hero Group 
        $hero_field_values = array(
        $hero_image_key => $rei_hero_image_ID,
        $hero_title_key => $reiHeroTitle,
        $hero_subtitle_key => $rei_hero_subtitle,
        $hero_content_key => $rei_hero_content,
        );
        update_field($HeroGroupKey, $hero_field_values, $reiPageId);
        
         
        //main content group
        if($reiMainContentTitle){
        $MainContentGroupKey = $field_objects['reim_main']['key'];
        $mainContentTitleKey = $field_objects['reim_main']['sub_fields'][0]['key'];
        $mainContentKey = $field_objects['reim_main']['sub_fields'][1]['key']; 
        //Update the Main Content Group
        $mainContent_values = array(
        $mainContentKey => $reiMainContent,
        $mainContentTitleKey => $reiMainContentTitle,
        ); 
        update_field($MainContentGroupKey, $mainContent_values, $reiPageId);
        
        } else {
        //main content default group
        $reiDefaultPageGroupKey = $field_objects['reim_default_main']['key'];
        $reiDefaultPageContentKey = $field_objects['reim_default_main']['sub_fields'][0]['key'];
         //Update the Default Pages
        $rei_defaultpage_values = array(
        $reiDefaultPageContentKey => $reiMainContent,
        ); 
        update_field($reiDefaultPageGroupKey, $rei_defaultpage_values, $reiPageId); 
        }
        
        
         
         //Add Form Group 
        $AddFormGroupKey = $field_objects['rei_add_form']['key'];
        $rei_add_form_title_key = $field_objects['rei_add_form']['sub_fields'][1]['key'];
        $rei_add_form_content_key = $field_objects['rei_add_form']['sub_fields'][3]['key'];

 
         
        // update Add Form Group 
        $add_form_field_values = array(
        $rei_add_form_title_key => $rei_add_form_title,
        $rei_add_form_content_key => $rei_add_form_content,
            
        );
        update_field($AddFormGroupKey, $add_form_field_values, $reiPageId);
         
         
        //quicklinks group
        $QuicklinksGroupKey = $field_objects['rei_quicklinks']['key'];
        $QuicklinksTitleKey = $field_objects['rei_quicklinks']['sub_fields'][0]['key'];
        $QuicklinksContentKey = $field_objects['rei_quicklinks']['sub_fields'][1]['key'];
        
         //Update the Quicklinks Group
        $rei_quicklinks_values = array(
        $QuicklinksTitleKey => $reiQuicklinksTitle,
        $QuicklinksContentKey => $reiQuicklinksContent,
        );
         
        update_field($QuicklinksGroupKey, $rei_quicklinks_values, $reiPageId); 
         
         
        //projects group
        $ProjectsGroupKey = $field_objects['rei_home_projects']['key'];
        $ProjectsTitleKey = $field_objects['rei_home_projects']['sub_fields'][1]['key'];
        $ProjectsSubTitleKey = $field_objects['rei_home_projects']['sub_fields'][2]['key'];
        $ProjectsContentKey = $field_objects['rei_home_projects']['sub_fields'][3]['key']; 
        //Update the Projects Group
        $rei_projects_values = array(
        $ProjectsTitleKey => $reiProjectsTitle,
        $ProjectsSubTitleKey => $reiProjectsSubTitle,
        $ProjectsContentKey => $reiProjectsContent,
        );
         
        update_field($ProjectsGroupKey, $rei_projects_values, $reiPageId);  
         
        //reviews group
        $ReviewsGroupKey = $field_objects['reim_home_reviews']['key'];
        $ReviewsTitleKey = $field_objects['reim_home_reviews']['sub_fields'][1]['key'];
        $ReviewsSubTitleKey = $field_objects['reim_home_reviews']['sub_fields'][2]['key'];
        $ReviewsContentKey = $field_objects['reim_home_reviews']['sub_fields'][3]['key']; 
        //Update the Reviews Group
        $rei_reviews_values = array(
        $ReviewsTitleKey => $reiReviewsTitle,
        $ReviewsSubTitleKey => $reiReviewsSubTitle,
        $ReviewsContentKey => $reiReviewsContent,
        );
         
        update_field($ReviewsGroupKey, $rei_reviews_values, $reiPageId); 
         
         
        
        //Questions/FAQ group
        $FaqGroupKey = $field_objects['reim_home_faq']['key'];
        $FaqTitleKey = $field_objects['reim_home_faq']['sub_fields'][0]['key'];
        $FaqQuestions1Key = $field_objects['reim_home_faq']['sub_fields'][1]['key'];
        $FaqAnswers1Key = $field_objects['reim_home_faq']['sub_fields'][2]['key'];
        $FaqQuestions2Key = $field_objects['reim_home_faq']['sub_fields'][3]['key'];
        $FaqAnswers2Key = $field_objects['reim_home_faq']['sub_fields'][4]['key'];
        $FaqQuestions3Key = $field_objects['reim_home_faq']['sub_fields'][5]['key'];
        $FaqAnswers3Key = $field_objects['reim_home_faq']['sub_fields'][6]['key'];

         
        
        //Update the Questions/FAQ Group
        $rei_faq_values = array(
        $FaqTitleKey => $reiFaqTitle,
        $FaqQuestions1Key => $reiFaqQuestions[0],
        $FaqAnswers1Key => $reiFaqAnswers[0],
        $FaqQuestions2Key => $reiFaqQuestions[1],
        $FaqAnswers2Key => $reiFaqAnswers[1],
        $FaqQuestions3Key => $reiFaqQuestions[2],
        $FaqAnswers3Key => $reiFaqAnswers[2],
        ); 
        update_field($FaqGroupKey, $rei_faq_values, $reiPageId);
         
         
        /* 
        //main content default group
        $reiDefaultPageGroupKey = $field_objects['reim_default_main']['key'];
        $mainDefaultPageContentKey = $field_objects['reim_default_main']['sub_fields'][0]['key'];
         //Update the Default Pages
        $rei_defaultpage_values = array(
        $mainDefaultPageContentKey => $reiMainContent,
        ); 
        update_field($reiDefaultPageGroupKey, $rei_defaultpage_values, $reiPageId); 
         */
         
   wp_die();   
 }
}
?>