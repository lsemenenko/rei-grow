( function( $ ) {
// validate the comment form when it is submitted
$('.send-report').click( function(){

    var values = $('#idb_rpp_contact_form').serializeArray();
    var $button = $( this );

    // set ajax data
    var data = {
        'action' : 'idb_rpp_resume',
        'post_id': $button.data('post_id'),
        'nonce'  : $button.data('nonce'),
        'form_contents' : values,
    };

    $('#idb_rpp_contact_form').validate({
    submitHandler: function(form) {
        $.post({
            url: settings.ajaxurl,
            data: data,
            success: function(response) {
                console.log(response);
                 alert("Success!");
            }            
        });
    },

        
});
});

})( jQuery );