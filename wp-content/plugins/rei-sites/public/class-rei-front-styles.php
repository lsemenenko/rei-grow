<?php
class Idb_RPP_loadstyles {

    function __construct() {
            add_action( 'wp_enqueue_scripts', array($this, 'idb_rpp_enqueue_contact_scripts' ));
            add_action( 'wp_enqueue_scripts', array($this, 'idb_rei_enqueue_styles' ));
            //ajax
            add_action( 'wp_ajax_rpp_resume_ajax_hook', 'rpp_resume_ajax_hook' );
        }
    
    function rpp_do_acf_form_head() {
         if(is_user_logged_in() && current_user_can('publish_posts')){
	           acf_form_head();
             wp_deregister_style( 'wp-admin' );     
         }
        }
    
    function idb_rei_enqueue_styles() {
        wp_enqueue_style( 'idb_rpp_edit', REI_PLUGIN_URL .'public/css/rei-sites-editor-frontend.css?v=1.4' );
    }
                  
    function idb_rpp_enqueue_scripts() {
            
            if(is_page() && is_user_logged_in() && current_user_can('publish_posts')){


            wp_enqueue_script( 'idb_denta_bio_page', REI_PLUGIN_URL .'public/js/idb_bio_page.js', array('jquery'), null, true );  

                
            } // end if page
        }

    function idb_rpp_enqueue_contact_scripts() {
                
            
            if( is_user_logged_in() && current_user_can('publish_posts') ){
                     
                wp_enqueue_media();    
                
            wp_enqueue_script( 'rpp-resume-ajax-handle',  REI_PLUGIN_URL . 'public/ajax-save-post/idb_ajax_edit_resume.js', array('jquery'), '1.0', true );

            // set variables for Ajax script
            wp_localize_script( 'rpp-resume-ajax-handle', 'resume_settings', array(
                'rpp_resume_ajax_security' => wp_create_nonce('idb-rpp-resume-security-nonce'),
                 'resume_ajaxurl'    => admin_url( 'admin-ajax.php' )
            ) );
            
             }
                
                }
    
    }//endclass


new Idb_RPP_loadstyles;