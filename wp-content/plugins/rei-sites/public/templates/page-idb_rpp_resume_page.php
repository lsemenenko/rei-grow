<?php
/**
 * The template for resume.
 *
 * This is the template that displays resume.
 *
 *
 * @package Astra
 * @since 1.0.0
 */

?>

  <style>
            #page {
                background: <?php echo get_field('idp_rpp_background_color', 'option');?>;
            }
        </style>

<?php

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

        
		<?php astra_content_page_loop(); ?>

		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
