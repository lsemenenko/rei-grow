<?php
/**
 * A class that handles loading custom modules and custom
 * fields if the builder is installed and activated.
 */
class Rei_Sites_Beaver_Modules_Loader {
	
	/**
	 * Initializes the class once all plugins have loaded.
	 */
	static public function init() {
		add_action( 'plugins_loaded', __CLASS__ . '::setup_hooks' );
	}
	
	/**
	 * Setup hooks if the builder is installed and activated.
	 */
	static public function setup_hooks() {
		if ( ! class_exists( 'FLBuilder' ) ) {
			return;	
		}
		
		// Load custom modules.
		add_action( 'init', __CLASS__ . '::load_modules' );
        add_filter('acf/load_field/name=_post_title', __CLASS__ . '::wd_post_title_acf_name');
		
	}
	
	/**
	 * Loads our custom modules.
	 */
	static public function load_modules() {
		require_once REI_MODULES_DIR . '/review-form/review-form.php';
		require_once REI_MODULES_DIR . '/example/example.php';
	}
    
    
    // Modify ACF Form Label for Post Title Field
static public function wd_post_title_acf_name( $field ) {
    
          $field['label'] = 'Review Title';
 
     return $field;
}

	
} //end class