<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://idbmedia.com
 * @since      1.0.0
 *
 * @package    Rei_Sites
 * @subpackage Rei_Sites/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Rei_Sites
 * @subpackage Rei_Sites/public
 * @author     J Hanlon - IDB Media <j@roselane.com>
 */
class Rei_Sites_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}
    

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/rei-sites-frontend-form.css?v=2.0.2', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

        wp_enqueue_script( 'sidr',  plugin_dir_url( __FILE__ ) . 'js/jquery.sidr.min.js', array( 'jquery' ), '1.2.1', true );

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/rei-sites-frontend-form.js', array( 'jquery' ), $this->version, true );

	}

}

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            height: 90px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
