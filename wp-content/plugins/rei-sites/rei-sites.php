<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://idbmedia.com
 * @since             1.1.0
 * @package           Rei_Sites
 *
 * @wordpress-plugin
 * Plugin Name:       REI Sites
 * Plugin URI:        https://idbmedia.com
 * Description:       Custom REI Sites functionality.
 * Version:           1.1.0
 * Author:            WaaS Hero - IDB Media
 * Author URI:        https://waashero.com
 * Text Domain:       rei-sites
 * Domain Path:       /languages
 */

    // If this file is called directly, abort.
    if ( ! defined( 'WPINC' ) ) {
        die;
    }

    define( 'REI_SITES_VERSION', '1.1.0' );
    define( 'REI_PLUGIN_URL', plugins_url( '/', __FILE__ ));

    /**
     * The code that runs during plugin activation. NOT USED. May use in future.
     * This action is documented in includes/class-rei-sites-activator.php
     */
    // function activate_rei_sites() {
    //     require_once plugin_dir_path( __FILE__ ) . 'includes/class-rei-sites-activator.php';
    //     Rei_Sites_Activator::activate();
    // }

    /**
     * The code that runs during plugin deactivation. NOT USED. May use in future.
     * This action is documented in includes/class-rei-sites-deactivator.php
     */
    // function deactivate_rei_sites() {
    //     require_once plugin_dir_path( __FILE__ ) . 'includes/class-rei-sites-deactivator.php';
    //     Rei_Sites_Deactivator::deactivate();
    // }

    //register_activation_hook( __FILE__, 'activate_rei_sites' );
    //register_deactivation_hook( __FILE__, 'deactivate_rei_sites' );
 
    function rei_admin_notice__error() {
        $class = 'notice notice-error';
        $message = __( 'REI Sites plugin requires Acf PRO. Please install the Advanced Custom Field PRO plugin.', 'rei-sites' );
     
        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
    }

    /**
     * The core plugin class that is used to define internationalization,
     * admin-specific hooks, and public-facing site hooks.
     */
    require plugin_dir_path( __FILE__ ) . 'includes/class-rei-sites.php';

    /**
     * Begins execution of the plugin.
     *
     * Since everything within the plugin is registered via hooks,
     * then kicking off the plugin from this point in the file does
     * not affect the page life cycle.
     *
     * @since    1.0.0
     */
    function run_rei_sites() {

        if (!class_exists('ACF')) {
            add_action( 'network_admin_notices', 'rei_admin_notice__error' );
            add_action( 'admin_notices', 'rei_admin_notice__error' );
            return;
        }
        $plugin = new Rei_Sites();
        $plugin->run();

    }

    //run_rei_sites();
    add_action('plugins_loaded', 'run_rei_sites');


    /**
     * Adds Google Tag Manager code to Ultimo's signup.
     *
     * This can also be used to insert other JavaScript integration snippets.
     *
     * @author Arindo Duque - NextPress
     * @since 0.0.1
     *
     */
    /**
     * Adds the main GTM code to the header.
     *
     * @return void
     */
    function wu_add_gtm_code() { ?>

    <!-- MAIN GTM CODE BELOW THIS LINE -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5VM3JDG');</script>
    <!-- END GTM CODE -->

    <?php } // end wu_add_gtm_code;
    add_action('signup_header', 'wu_add_gtm_code');
    /**
     * Adds GTM code after the body tag.
     *
     * @return void
     */
    function wu_add_gtm_body_code() { ?>

    <!-- GTM AFTER BODY CODE BELOW THIS LINE -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VM3JDG"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- END GTM CODE -->

    <?php } // end wu_add_gtm_body_code;
    add_action('wu_signup_header', 'wu_add_gtm_body_code');

    add_filter( 'gform_display_add_form_button', function(){return false;} );

    // DNS Instructions Replace Text
    add_filter(  'gettext',  'wps_translate_words_array'  );
    add_filter(  'ngettext',  'wps_translate_words_array'  );
    function wps_translate_words_array( $translated ) {
    
        $words = array(
                // 'word to translate' = > 'translation'
                'Point an A Record to the following IP Address' => 'Please make your DNS changes by following <a href="https://help.reigrow.com/investor-knowledge-base/setting-up-a-custom-domain/" target="_blank">"Add Custom Domain"</a>',
            'You can also create a CNAME record on your domain pointing to our domain' => 'Wait at least 30 minutes and then set your custom domain',
                );
    
        $translated = str_ireplace(  array_keys($words),  $words,  $translated );
        return $translated;
    }


    // Force Gravity Form Notifications From Address 
    add_filter( 'gform_notification', 'change_from_email', 10, 3 );
    function change_from_email( $notification, $form, $entry ) {
        $notification['from'] = 'leads@reigrow.com'; 
        return $notification;
    }

    // Remove Default Wordpress Welcome Panel on Dashboard
    remove_action('welcome_panel', 'wp_welcome_panel');


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Site Settings',
		'menu_title'	=> 'Site Settings',
		'menu_slug' 	=> 'acf-options-site-options',
		'redirect'		=> false
	));
}