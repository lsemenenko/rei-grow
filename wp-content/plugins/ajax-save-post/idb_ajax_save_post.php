<?php
if (is_admin()){
 // AJAX ADD ACTIONS
 add_action( 'wp_ajax_idb_rpp_ajax_hook', 'idb_rpp_ajax_hook' );
  
    
    /*
function get_attachment_id( $url ) {
	$attachment_id = 0;
	$dir = wp_upload_dir();
	if ( false !== strpos( $url, $dir['baseurl'] . '/' ) ) { // Is URL in uploads directory?
		$file = basename( $url );
		$query_args = array(
			'post_type'   => 'attachment',
			'post_status' => 'inherit',
			'fields'      => 'ids',
			'meta_query'  => array(
				array(
					'value'   => $file,
					'compare' => 'LIKE',
					'key'     => '_wp_attachment_metadata',
				),
			)
		);
		$query = new WP_Query( $query_args );
		if ( $query->have_posts() ) {
			foreach ( $query->posts as $post_id ) {
				$meta = wp_get_attachment_metadata( $post_id );
				$original_file       = basename( $meta['file'] );
				$cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
				if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
					$attachment_id = $post_id;
					break;
				}
			}
		}
	}
	return $attachment_id;
} */
    
     // idb Ajax function
     function idb_rpp_ajax_hook(){
        //Ajax Security Nonce
        if ( ! check_ajax_referer( 'idb-rpp-security-nonce', 'rpp_ajax_security' ) && current_user_can('edit_others_pages') ) {
        wp_send_json_error( 'Invalid security token sent.' );
        wp_die();
        }
          // save repeater field values
        $bio_repeater = "field_5bcacee2ad04a";
        $bio_text_sub_field = "idb_rpp_bio";
        $bio_image_sub_field = "idb_rpp_photo";
        if (isset($_POST[ 'idbBioData' ])){
         $idb_bio_text_array = $_POST[ 'idbBioData' ];
         }
        if (isset($_POST[ 'idbBioImage' ])){
         $idb_bio_image_array = $_POST[ 'idbBioImage' ];
        }

       
    // ACF rows start at 1 not 0
    $row = 1;

     while ( have_rows($bio_repeater,'option') ) : the_row();
         
      foreach ($idb_bio_text_array as $bio_data) {   
    update_sub_field( array( $bio_repeater, $row, $bio_text_sub_field ), $bio_data,'option');
    // adds row numbers     
     $row++;     
      }  
     
     endwhile;
         
         $image_row = 1;
     while ( have_rows($bio_repeater,'option') ) : the_row();    
            foreach ($idb_bio_image_array as $bio_images) {  
         $postid_img = attachment_url_to_postid($bio_images);  
    update_sub_field( array( $bio_repeater, $image_row, $bio_image_sub_field ), $postid_img,'option');
    // adds row numbers     
     $image_row++;     
      }      
      endwhile;    
         
         
   wp_die();   
 }
}
?>