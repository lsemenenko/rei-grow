(function($) {
    
    /*
     $(".editable-text-full").editable({
     multiline: true,
     className: "ui-editable",
     editingClass: "ui-editable-editing",
invalidClass: "ui-editable-invalid",
     //whether to automatically select all content when starting editing
  autoselect: false,

 });
 */
/*
ClassicEditor
    .create( document.querySelector( '#editor' ) )
    .then( newEditor => {
        editor = newEditor;
    } )
    .catch( error => {
        console.error( error );
    } );
    */

    

$(document).ready( function() {
    
   
    var target; // variable for event target to get id of clicked element
	var file_frame; // variable for the wp.media file_frame
	
	// attach a click event (or whatever you want) to some element on your page
	$( '.idb-denta-avatar' ).on( 'click', function( event ) {
        var $this = $(this); 
        target = event.target.id;
		event.preventDefault();

        // if the file_frame has already been created, just reuse it
		if ( file_frame ) {
			file_frame.open();
			return;
		} 

		file_frame = wp.media.frames.file_frame = wp.media({
			title: $this.data( 'uploader_title' ),
			button: {
				text: $this.data( 'uploader_button_text' ),
			},
			multiple: false // set this to true for multiple file selection
		});

		file_frame.on( 'select', function() {
			attachment = file_frame.state().get('selection').first().toJSON();

			$("#" +target).css("background-image", "url(" + attachment.url + ")");
          
		});

		file_frame.open();
	});
});
    
    
    
    var testMyBtn = document.getElementById("submit-2"); 
    
    if(testMyBtn){ 
    testMyBtn.addEventListener("click", function(){ 
        const divsContents = [...document.querySelectorAll(".idb-bio-content")].map(e=>e.innerText.trim());
        const imageContents = [...document.querySelectorAll(".idb-denta-avatar")].map(e=>e.style.backgroundImage.replace(/^url\(['"](.+)['"]\)/, '$1'));    
        //console.log("Button Clicked");
        //console.log(divsContents);
        
        var str = {
			'action': 'idb_denta_ajax_hook',
			'idbBioData': divsContents,
            'idbBioImage': imageContents,
            'denta_ajax_security' : settings.denta_ajax_security,
		};
            $.ajax({
        type: 'POST', 
        url: settings.ajaxurl, 
             dataType: "html",   
                 data : str,
                success: function(result) {
                 
                    $(testMyBtn).text('Submitted');
                       
                   
                     //console.log('success');
                    //console.log(divsContents);
                     //console.log(imageContents);
                    
                },
                error: function(result) {
                    
                     console.log('Somethings messed up!');
                    
                }
         })
    })
        }
/*
	   $('#editor-*').blur(function(){
    var myTxt = $('#editor').text();

    var details2 = document.getElementsByClassName("idb-denta-bio-container")[0];
    var biolove = details2.getElementsByClassName("idb-bio-content")[0].innerHTML;
           
 console.log(biolove);

        console.log("Button Clicked");
        
        var str = {
			'action': 'idb_denta_ajax_hook',
			'idbBioData': biolove,
		};
            $.ajax({
        type: 'POST', 
        url: settings.ajaxurl, 
             dataType: "html",   
                 data : str,
                success: function(result) {
                   $( ".ui-editable-editing" ).append( "<div class='idb-denta-ajax-success'><p>Saved</p></div>" );
                     console.log('success');
                    console.log(biolove);
                    
                },
                error: function(result) {
                    
                     console.log('somethings messed up');
                    
                }
         })
           
	});
    */
})(jQuery);